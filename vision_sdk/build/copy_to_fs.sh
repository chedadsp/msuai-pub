
#!/bin/bash
set -e

if [ $# -eq 0 ]
  then
    echo "Usage ./copy_to_fs.sh /media/rtrk/rootfs (use lsblk to check where rootfs is mounted)."
    exit
fi

echo "Firmware copy in progress..."
cp -r ../../ti_components/os_tools/linux/targetfs/lib/firmware $1/lib/
echo "Vision SDK copy in progress..."
cp -r ../../ti_components/os_tools/linux/targetfs/opt/vision_sdk $1/opt/
echo "Done!"

exit
