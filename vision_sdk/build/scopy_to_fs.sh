
#!/bin/bash
set -e

if [ $# -eq 0 ]
  then
    echo "Usage ./scopy_to_fs.sh IP_ADDRESS (use ifconfig on alpha to see ip)."
    exit
fi

echo "Firmware copy in progress..."
scp -r ../../ti_components/os_tools/linux/targetfs/lib/firmware root@$1:/lib/
echo "Vision SDK copy in progress..."
scp -r ../../ti_components/os_tools/linux/targetfs/opt/vision_sdk root@$1:/opt/
echo "Done!"

exit
