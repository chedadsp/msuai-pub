/*******************************************************************************
 *
 * Copyright (c) 2019  RT-RK Automotive d.o.o
 * Narodnog fronta 23 D-E, 21000 Novi Sad, Serbia
 *
 * All Rights Reserved
 *
 * P R O P R I E T A R Y & C O N F I D E N T I A L
 *
 * -----------------------------------------------------
 * http://www.rt-rk.com
 * -----------------------------------------------------
 *
 ******************************************************************************/

/**
 *******************************************************************************
 * @file frameGrabLink.h
 * 
 * @author Uros Zivkovic (uros.zivkovic@rt-rk.com)
 * 
 * @brief Frame Grab Link API public header file.
 * 
 * @version 0.0 (Oct 2018) : [MD] First version
 * @version 0.1 (Mar 2019) : [UZ] Updated file structure and
 *                                adjusted file contingency
 * 
 * @date 24-03-2019
 * 
 * @copyright Copyright (c) 2019 RT-RK Automotive, All rights reserved.
 * 
 *******************************************************************************
 */

#ifndef FRAMEGRAB_LINK_H_
#define FRAMEGRAB_LINK_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include <include/link_api/system.h>

/*******************************************************************************
 *  Defines
 *******************************************************************************
 */

/* @{ */

 /**
 *******************************************************************************
 *
 * \brief Maximum number of FrameGrab link objects
 *
 * SUPPORTED in ALL platforms
 *
 *******************************************************************************
 */
#define FRAMEGRAB_LINK_OBJ_MAX      (6U)

/**
*******************************************************************************
 *
 * \brief Max output queues supported by Null Link
 *
 * SUPPORTED in ALL platforms
 *

*******************************************************************************
*/
#define FRAMEGRAB_LINK_MAX_IN_QUE   (1U)

/* @} */


/******************************************************************************
 *
 *  Data structures
 *
*******************************************************************************
*/

/**
 ******************************************************************************
 * 
 * \brief FrameGrab Link create parameters
 *
*******************************************************************************
*/
typedef struct
{
    UInt32  numInQue;
    /**< Number of input queues */

    System_LinkInQueParams   inQueParams;
    /**< Input queue information */

    char semPath[32];
    /**< Path to semaphore created for sync on shm */

    key_t shmKey;
    /**< Shared memory key */

    UInt32 defaultCh;
    /**< Default chanel from wich frames are acquired */

} FrameGrabLink_CreateParams;

/******************************************************************************
*
*  Functions
*
*******************************************************************************
*/
static inline void FrameGrabLink_CreateParams_Init(FrameGrabLink_CreateParams *pPrm);

/**
*******************************************************************************
 *
 * \brief FrameGrab link register and init
 *
 *    - Creates link task
 *    - Registers as a link with the system API
 *
 * \return  SYSTEM_LINK_STATUS_SOK
 *
 *******************************************************************************
 */
Int32 FrameGrabLink_init(void);

/**
*******************************************************************************
 *
 * \brief FrameGrab link de-register and de-init
 *
 *    - Deletes link task
 *    - De-registers as a link with the system API
 *
 * \return  SYSTEM_LINK_STATUS_SOK
 *
 *******************************************************************************
 */
Int32 FrameGrabLink_deInit(void);

/**
 *******************************************************************************
 *
 * \brief FrameGrab link set default parameters for create time params
 *   This function does the following
 *      - memset create params object
 * \param  pPrm  [OUT]  NullLink Create time Params
 *
 *******************************************************************************
 */
static inline void FrameGrabLink_CreateParams_Init(FrameGrabLink_CreateParams *pPrm)
{
/* MISRA.ONEDEFRULE.FUNC
 * MISRAC_2004 Rule 8.5
 * Function Definition in header file
 * KW State: Defer -> Waiver -> Case by case
 * MISRAC_WAIVER: This function initializes the create time argument to default.
 *  Defining this in the header file aids maintainability.
 */
    memset(pPrm, 0, sizeof(FrameGrabLink_CreateParams));

    pPrm->numInQue = 1U;

    return;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

/*@}*/
