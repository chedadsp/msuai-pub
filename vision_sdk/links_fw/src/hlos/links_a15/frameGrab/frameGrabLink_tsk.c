/*******************************************************************************
 *
 * Copyright (c) 2019  RT-RK Automotive d.o.o
 * Narodnog fronta 23 D-E, 21000 Novi Sad, Serbia
 *
 * All Rights Reserved
 *
 * P R O P R I E T A R Y & C O N F I D E N T I A L
 *
 * -----------------------------------------------------
 * http://www.rt-rk.com
 * -----------------------------------------------------
 *
 ******************************************************************************/

/**
 *******************************************************************************
 * @file frameGrabLink_tsk.c
 * 
 * @author Uros Zivkovic (uros.zivkovic@rt-rk.com)
 * 
 * @brief Frame Grab Link source file.
 *        Implements passing frames from VSDK to Linux using shared memory.
 * 
 * @version 0.0 (Oct 2018) : [MD] First version
 * @version 0.1 (Mar 2019) : [UZ] Updated file structure and
 *                                adjusted file contingency
 * 
 * @date 24-03-2019
 * 
 * @copyright Copyright (c) 2019 RT-RK Automotive, All rights reserved.
 * 
 *******************************************************************************
 */

/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include "frameGrabLink_priv.h"

/*****************************************************************************
 *  DEFINES
 *****************************************************************************
 */
#define WIDTH       (1280)
#define HEIGHT      (720)
#define HW_SIZE     (WIDTH * HEIGHT)
#define FRAME_SIZE  (HW_SIZE + HW_SIZE / 2)
/*******************************************************************************
 *  FUNCTION DECLARATIONS
 *******************************************************************************
 */
Int32 FrameGrabLink_drvCreate(FrameGrabLink_Obj * pObj,const FrameGrabLink_CreateParams * pPrm);
Int32 FrameGrabLink_drvProcessData(FrameGrabLink_Obj * pObj);

/**
 *******************************************************************************
 * \brief Link object, stores all link related information
 *******************************************************************************
 */
FrameGrabLink_Obj gFrameGrabLink_obj[FRAMEGRAB_LINK_OBJ_MAX];

UInt8 gFrameGrabLink_tskStack[FRAMEGRAB_LINK_OBJ_MAX][FRAMEGRAB_LINK_TSK_STACK_SIZE];

/**
 ******************************************************************************
 *
 * \brief FrameGrab link gets created using this function.
 *
 * Sets up link data structure, allocates and queue buffers. Makes link ready
 * for operation.
 *
 * \param  pObj     [IN] FrameGrabLink_Obj link global handle
 * \param  pPrm     [IN] FrameGrabLink_Obj create parameters
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 ******************************************************************************
 */
Int32 FrameGrabLink_drvCreate(FrameGrabLink_Obj * pObj,
                              const FrameGrabLink_CreateParams * pPrm)
{
    Int32 status;
    System_LinkInfo inTskInfo;

    memcpy(&pObj->createArgs, pPrm, sizeof(pObj->createArgs));

    /**Chanel in input queue from which frames are taken */
    pObj->defaultCh = pPrm->defaultCh;

    /**Setup shared memory key */
    pObj->shmkey = pPrm->shmKey;
    Vps_printf("[FrameGrab]: Shared memory key value: %d\n\n", pObj->shmkey);

    /**Setup shared memory semaphore */
    strcpy(pObj->semPath, pPrm->semPath);
    Vps_printf("Semaphore path: %s\n", pObj->semPath);

    pObj->shmid = shmget(pObj->shmkey, FRAME_SIZE, IPC_CREAT | 0666);
    if(pObj->shmid < 0)
    {
        perror("shmget failed: ");
        Vps_printf("Shared memory get failed!");
        OSA_assert(FALSE);
    }

    if ((pObj->shm = shmat(pObj->shmid, NULL, 0)) == (char *) -1)
    {
        perror("shmget failed: ");
        Vps_printf("Shm failed to attach!");
        OSA_assert(FALSE);
    }

    pObj->sem = sem_open(pObj->semPath, O_CREAT, 0644, 0);
    if(pObj->sem == SEM_FAILED)
    {
        perror("semopen failed: ");
        Vps_printf("Semaphore open failed!");
        OSA_assert(FALSE);
    }

    status = System_linkGetInfo(pPrm->inQueParams.prevLinkId, &inTskInfo);
    OSA_assert(status == SYSTEM_LINK_STATUS_SOK);

    OSA_assert(pPrm->inQueParams.prevLinkQueId < inTskInfo.numQue);
    pObj->inQueInfo = inTskInfo.queInfo[pPrm->inQueParams.prevLinkQueId];

    return SYSTEM_LINK_STATUS_SOK;
}

/**
 ******************************************************************************
 *
 * \brief Process the buffer data in response to a callback.
 *
 * This function gets called in response to a callback. It puts buffers into
 * link output queue and sends message to next link
 *
 * \param  pObj          [IN] FrameGrab link object
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 ******************************************************************************
 */
Int32 FrameGrabLink_drvProcessData(FrameGrabLink_Obj * pObj)
{
    System_LinkInQueParams *pInQueParams;
    System_BufferList bufList;
    UInt32 bufId;

    UInt32 offsetY, offsetUV;
    UInt32 temp32;

    pInQueParams = &pObj->createArgs.inQueParams;

    /**Acquire actual buffers */
    System_getLinksFullBuffers(pInQueParams->prevLinkId, 
                               pInQueParams->prevLinkQueId,
                               &bufList);

    if (bufList.numBuf)
    {
        pObj->recvCount += bufList.numBuf;

        for (bufId = 0; bufId < bufList.numBuf; bufId++)
        {

            System_Buffer *buff = bufList.buffers[bufId];

            /**[WARNING]: Capture buffers need to be set for SYSTEM_BUFFER_TYPE_VIDEO_FRAME */
            if(bufList.buffers[bufId]->bufType == SYSTEM_BUFFER_TYPE_VIDEO_FRAME
                && bufList.buffers[bufId]->chNum == pObj->defaultCh)
            {

                System_VideoFrameBuffer *videoFrame = (System_VideoFrameBuffer*)buff->payload;
                //System_VideoDataFormat videoFormat = (System_VideoDataFormat)
                System_Link_Ch_Info_Get_Flag_Data_Format(pObj->inQueInfo.chInfo[0].flags);

                /**If start */
                //TODO: Implement same buffer caching for RGB format 
                if ((videoFrame->chInfo.startX!=0) || (videoFrame->chInfo.startY!=0))
                {
                    offsetY  = ((videoFrame->chInfo.pitch[0]*videoFrame->chInfo.startY)
                             + videoFrame->chInfo.startX);
                    offsetUV = offsetY;

                    //TODO: Before calculating this offset, check if frame is YUV420SP
                    offsetUV = ((videoFrame->chInfo.pitch[1]*(videoFrame->chInfo.startY/2U))
                                 + videoFrame->chInfo.startX);

                    /**Y component */
                    temp32 = ((UInt32)videoFrame->bufAddr[0U] + offsetY);
                    videoFrame->bufAddr[0U] = (UInt8 *) (temp32);
                    /**UV components */
                    temp32 = ((UInt32)videoFrame->bufAddr[1U] + offsetUV);
                    videoFrame->bufAddr[1U] = (UInt8 *) (temp32); 
                }

                /**Copying acquired frame (Y and UV) components to shared memory segment */
                UInt32 i, j;
                j = 0;
                for(i = 0; i < (HEIGHT * 2) + 1; i+=2)
                {
                    memcpy(pObj->shm + (j * WIDTH), videoFrame->bufAddr[0] + (i * WIDTH), WIDTH);
                    j++;    
                }

                UInt32 offset = HW_SIZE;
                j = 0;

                for(i = 0; i < ((HEIGHT / 2) * 2) + 1; i+=2)
                {
                    memcpy(pObj->shm + offset + (j * WIDTH), videoFrame->bufAddr[1] + (i * WIDTH), WIDTH);
                    j++;
                }

                int rv;
                rv = sem_post(pObj->sem);
                if(rv < 0)
                {
                    perror("sem_post()");
                }
            }

        }

        System_putLinksEmptyBuffers(pInQueParams->prevLinkId,
                                   pInQueParams->prevLinkQueId, &bufList);
    }


    return SYSTEM_LINK_STATUS_SOK;
}

/**
 ******************************************************************************
 *
 * \brief This function is the implementation of Idle state.
 *
 * \param  pTsk [IN] Task Handle
 * \param  pMsg [IN] Message Handle
 *
 * \return  void
 *
 ******************************************************************************
 */
Int32 FrameGrabLink_tskMain(struct OSA_TskHndl * pTsk, OSA_MsgHndl * pMsg, UInt32 curState)
{
    UInt32 cmd = OSA_msgGetCmd(pMsg);
    Bool ackMsg, done;
    Int32 status = SYSTEM_LINK_STATUS_SOK;
    FrameGrabLink_Obj *pObj = (FrameGrabLink_Obj *) pTsk->appData;

    if (cmd != SYSTEM_CMD_CREATE)
    {
        OSA_tskAckOrFreeMsg(pMsg, SYSTEM_LINK_STATUS_EFAIL);
        status = SYSTEM_LINK_STATUS_EFAIL;
    }

    /**Create shared memory space */
    if(status == SYSTEM_LINK_STATUS_SOK)
    {
        status = FrameGrabLink_drvCreate(pObj, OSA_msgGetPrm(pMsg));

        OSA_tskAckOrFreeMsg(pMsg, status);
    }

    /**Processing loop */
    if (status == SYSTEM_LINK_STATUS_SOK)
    {

        done = (Bool)FALSE;
        ackMsg = (Bool)FALSE;

        while (!done)
        {
            status = OSA_tskWaitMsg(pTsk, &pMsg);
            if (status != SYSTEM_LINK_STATUS_SOK)
            {
                break;
            }

            cmd = OSA_msgGetCmd(pMsg);

            switch (cmd)
            {
                case SYSTEM_CMD_DELETE:
                    done = (Bool)TRUE;
                    ackMsg = (Bool)TRUE;

                    memcpy(pObj->shm, "x\0", 1);
                    sem_post(pObj->sem);
                    shmdt(pObj->shm);
                    sem_close(pObj->sem);
                    //sem_unlink(pObj->semPath);
                    break;
                case SYSTEM_CMD_NEW_DATA:
                    OSA_tskAckOrFreeMsg(pMsg, status);
                    FrameGrabLink_drvProcessData(pObj);
                    break;
                default:
                    OSA_tskAckOrFreeMsg(pMsg, status);
                    break;
            }
        }

        if ((ackMsg != (Bool)FALSE) && (pMsg != NULL))
        {
            OSA_tskAckOrFreeMsg(pMsg, status);
        }
    }

    return status;
}

/**
 *******************************************************************************
 *
 * \brief Init function for FrameGrab link. This function registers this link
 *  from the system
 *
 * \return  SYSTEM_LINK_STATUS_SOK
 *
 *******************************************************************************
 */
Int32 FrameGrabLink_init(void)
{
    Int32 status;
    System_LinkObj linkObj;
    UInt32 fgId;
    FrameGrabLink_Obj *pObj;
    UInt32 procId = System_getSelfProcId();

    Vps_printf("|**************|Frame Grab Link Init|**************|");

    for (fgId = 0; fgId < FRAMEGRAB_LINK_OBJ_MAX; fgId++)
    {
        pObj = &gFrameGrabLink_obj[fgId];

        memset(pObj, 0, sizeof(*pObj));

        pObj->tskId = SYSTEM_MAKE_LINK_ID(procId,
                                          SYSTEM_LINK_ID_FRAMEGRAB_LINK_0 + fgId);
        memset(&linkObj, 0, sizeof(linkObj));
        linkObj.pTsk = &pObj->tsk;
        linkObj.linkGetFullBuffers= NULL;
        linkObj.linkPutEmptyBuffers= NULL;
        linkObj.getLinkInfo = NULL;


        System_registerLink(pObj->tskId, &linkObj);

        snprintf(pObj->tskName, 32, "FrameGrabber_%u", (unsigned int)fgId);
        status = OSA_tskCreate(&pObj->tsk,
                                 FrameGrabLink_tskMain,
                                 FRAMEGRAB_LINK_TSK_PRI,
                                 FRAMEGRAB_LINK_TSK_STACK_SIZE,
                                 0,
                                 pObj,
                                 pObj->tskName);
        OSA_assert(status == SYSTEM_LINK_STATUS_SOK);
        Vps_printf(" %s : Init done - linkid-0x%x\n", pObj->tskName, pObj->tskId);
    }

    Vps_printf("|************|Frame Grab Link Init Done|************|");
    return status;
}

/**
 *******************************************************************************
 *
 * \brief De-init function for FrameGrab link. This function de-registers this link
 *  from the system
 *
 * \return  SYSTEM_LINK_STATUS_SOK
 *
 *******************************************************************************
 */
Int32 FrameGrabLink_deInit(void)
{
    UInt32 pcieId;

    for (pcieId = 0; pcieId < FRAMEGRAB_LINK_OBJ_MAX; pcieId++)
    {
        OSA_tskDelete(&gFrameGrabLink_obj[pcieId].tsk);
    }

    return SYSTEM_LINK_STATUS_SOK;
}
