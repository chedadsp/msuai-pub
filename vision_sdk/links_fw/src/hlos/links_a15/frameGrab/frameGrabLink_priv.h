/*******************************************************************************
 *
 * Copyright (c) 2019  RT-RK Automotive d.o.o
 * Narodnog fronta 23 D-E, 21000 Novi Sad, Serbia
 *
 * All Rights Reserved
 *
 * P R O P R I E T A R Y & C O N F I D E N T I A L
 *
 * -----------------------------------------------------
 * http://www.rt-rk.com
 * -----------------------------------------------------
 *
 ******************************************************************************/

/**
 *******************************************************************************
 *
 * \ingroup FRAMEGRAB_LINK_API
 * \defgroup FRAMEGRAB_LINK_IMPL FrameGrab Link Implementation
 *
 * @{
 */

/**
 *******************************************************************************
 * @file frameGrabLink_priv.h
 * 
 * @author Uros Zivkovic (uros.zivkovic@rt-rk.com)
 * 
 * @brief Frame Grab Link API private header file.
 *        Implements passing frames from VSDK to Linux using shared memory.
 * 
 * @version 0.0 (Oct 2018) : [MD] First version
 * @version 0.1 (Mar 2019) : [UZ] Updated file structure and
 *                                adjusted file contingency
 * 
 * @date 24-03-2019
 * 
 * @copyright Copyright (c) 2019 RT-RK Automotive, All rights reserved.
 * 
 *******************************************************************************
 */

#ifndef FRAMEGRAB_LINK_PRIV_H_
#define FRAMEGRAB_LINK_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 *  Include files
 *******************************************************************************
 */
#include "links_fw/include/link_api/frameGrabLink.h"

#include <links_fw/src/hlos/osa/include/osa.h>
#include <links_fw/src/hlos/osa/include/osa_tsk.h>
#include <links_fw/src/hlos/osa/include/osa_mem.h>
#include <links_fw/src/hlos/system/system_priv_common.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <osa_cache.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <sys/stat.h>

/*******************************************************************************
 *  Defines
 *******************************************************************************
 */

/**
 *******************************************************************************
 * \brief Size FrameGrab link stack
 *******************************************************************************
 */
#define FRAMEGRAB_LINK_TSK_STACK_SIZE   (OSA_TSK_STACK_SIZE_DEFAULT)
#define FRAMEGRAB_LINK_TSK_PRI          (12U)

/*******************************************************************************
 *  Data structure's
 *******************************************************************************
 */

/**
 *******************************************************************************
 *
 * \brief Structure to hold all FrameGrab link related information
 *
 *******************************************************************************
 */

typedef struct {
    UInt32 tskId;
    /**< Placeholder to store FrameGrab link task id */

    OSA_TskHndl tsk;
    /**< Handle to FrameGrab link task */

    char tskName[32];
    /**< Link task name */

    FrameGrabLink_CreateParams createArgs;
    /**< Create params for FrameGrab link */

    System_LinkQueInfo  inQueInfo;
    /**< Input Queue info */

    UInt32 recvCount;
    /**< Count of buffers received */

    Int32 shmid;
    /**< Shared memory Id */

    key_t shmkey;
    /**< Shared memory key */

    char *shm;
    /**< Shared memory pointer */

    char semPath[32];
    /**< Sem path */

    sem_t *sem;
    /**< Semaphore for shared memory access */

    UInt32 defaultCh;
    /**< Default channel from wich frame buffers will be acquired */

} FrameGrabLink_Obj;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

/* @} */