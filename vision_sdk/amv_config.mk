#*******************************************************************************
# \file amv_config.mk
#
# \version 0.0 (Sep 2017) : [PS] First version
#
#*******************************************************************************

#*******************************************************************************
#
# \Defines wich TDA2xx is used. Choose one of following:
#
#           SCV
#           FFN
#           FUS
#
#*******************************************************************************
CHIP_TYPE=SC

#*******************************************************************************
#
# \Defines is Alpha board R003 used (yes/no)
# \Note: For board revision R004 also use ALPHA_R003=yes
#
#*******************************************************************************
ALPHA_R003=yes

#*******************************************************************************
#
# \Chip IP addresses for NDK_config.cfg
#
#*******************************************************************************
#SCV_IP=192.168.100.101
#FFN_IP=192.168.100.102
#FUS_IP=192.168.100.103

#*******************************************************************************
#
# \CCFLAGS based on AMV config

# \To include new defines in build, add them in amv_config_CCFLAGS
#
#*******************************************************************************
ifeq ($(CHIP_TYPE),SCV)
  amv_config_CCFLAGS += -DSCV_CHIP
endif

ifeq ($(CHIP_TYPE),FFN)
  amv_config_CCFLAGS += -DFFN_CHIP
endif

ifeq ($(CHIP_TYPE),FUS)
  amv_config_CCFLAGS += -DFUS_CHIP
endif

ifeq ($(ALPHA_R003),yes)
  amv_config_CCFLAGS += -DALPHA_R003
endif

#amv_config_CCFLAGS += -DSCV_IP="\"$(SCV_IP)\""
#amv_config_CCFLAGS += -DFFN_IP="\"$(FFN_IP)\""
#amv_config_CCFLAGS += -DFUS_IP="\"$(FUS_IP)\""

#*******************************************************************************
#
# \Export variables

# \If you add new variables you need to export them
#
#*******************************************************************************
export CHIP_TYPE
#export SCV_IP
#export FFN_IP
#export FUS_IP
export amv_config_CCFLAGS
export ALPHA_R003
