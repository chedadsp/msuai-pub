/*
 *   Copyright (c) RT-RK     2017
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of RT-RK nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *******************************************************************************
 *
 * \file chains_common_amv.h
 *
 * \brief This file contains common utility functions used by all AMV use-cases
 *
 *******************************************************************************
 */

#ifndef _CHAINS_COMMON_AMV_H_
#define _CHAINS_COMMON_AMV_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include <src/include/chains.h>
#include "include/link_api/system_common.h"
#include <ti/drv/vps/include/devices/bsp_amv_hdmi.h>
#include <ti/drv/vps/include/devices/bsp_amv_cam_PoC.h>
#include <ti/drv/vps/include/devices/bsp_amv_ov490.h>
#include <ti/drv/vps/include/devices/bsp_amv_i2c_switch.h>
#include <ti/drv/vps/include/devices/bsp_amv_ub964.h>
#include <ti/drv/vps/include/devices/bsp_amv_ub914.h>
#include <ti/drv/vps/include/devices/bsp_amv_ub913.h>
#include <src/include/video_sensor_amv.h>
#include <src/hlos/common/appCtrlLink.h>

/*******************************************************************************
 *  Defines
 *******************************************************************************
 */


/**
 *******************************************************************************
 *
 * \brief Time in milliseconds required to UB914 deserializer power on
 *
 *******************************************************************************
 */
#define UB914_POWER_ON_TIME_MS        (30)


/**
 *******************************************************************************
 *
 * \brief Time in milliseconds required to UB964 deserializer power on
 *
 *******************************************************************************
 */
#define UB964_POWER_ON_TIME_MS        (250)


/**
 *******************************************************************************
 *
 * \brief Camera power on time in milliseconds
 *
 *******************************************************************************
 */
#define CAMERA_POWER_ON_TIME_MS       (20)


/**
 *******************************************************************************
 *
 * \brief Time in milliseconds required to enable 0V490 image processor
 *
 *******************************************************************************
 */
#define OV490_ENABLE_TIME_MS          (100)



/*******************************************************************************
 *  Enum's
 *******************************************************************************
 */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */


/**
 *******************************************************************************
 *
 * \brief Enables and starts all devices for CMS on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StartCMSDevices();


/**
 *******************************************************************************
 *
 * \brief Enables and starts all devices for SV on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StartSVDevices();

/**
 *******************************************************************************
 *
 * \brief Enables and starts all devices for Deserializer 1 on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_Start_PairTwoCam(AMV_VideoSensor_Id pSelectedPair);

/**
 *******************************************************************************
 *
 * \brief Enables and starts all devices on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StartSCDevices();


/**
 *******************************************************************************
 *
 * \brief Disables and stops all devices for SV on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StopSVDevices();


/**
 *******************************************************************************
 *
 * \brief Disables and stops all devices for CMS on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StopCMSDevices();


/**
 *******************************************************************************
 *
 * \brief Disables and stops all devices on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StopSCDevices();

/**
 *******************************************************************************
 *
 * \brief Disables and stops all devices on Deserializer 1 on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_Stop_PairTwoCam();

/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 1A port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_1A();


/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 1B port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_1B();


/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 2A port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_2A();

/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 3A port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_3A();

/**
 *******************************************************************************

 *
 * \brief Sets VIP pinmux for 5A port
 *
 *******************************************************************************
 */

Void ChainsCommon_Amv_SetVipPinMux_5A();

/**
 *******************************************************************************

 *
 * \brief Sets VIP pinmux for 6A port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_6A();

/**
 *******************************************************************************
 *
 * \brief Configure and starts devices on FFN TDA2x
 *
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_ConfigureFFNDevice(AMV_VideoSensor_Id sensorId);

/**
 *******************************************************************************
 *
 * \brief Configure devices on FFN TDA2x
 *

 *
 * \param  sensorId       [IN] Video sensor ID
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_ConfigureFFNDevices();


/**
 *******************************************************************************
 *
 * \brief Deconfigure and stops devices on FFN TDA2x
 *
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_DeconfigureFFNDevice(AMV_VideoSensor_Id sensorId);

/**
 *******************************************************************************
 *
 * \brief Stops capture device on FFN TDA2x
 *
 * \param
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */

Void ChainsCommon_Amv_Start_Camera(
                           Amv_ChipType chipType,
                           Chains_CaptureSrc pCaptureSrc,
                           CaptureLink_CreateParams *pPrm,
                           UInt32 captureInWidth,
                           UInt32 captureInHeight,
                           AMV_VideoSensor_Id portId[],
                           UInt32 numOfSensor,
                           Bsp_VidSensorConfigRegs **vidSensorConfigRegs,
                           UInt32 *vidSensorConfigRegsSizes,
                           Bsp_VidSensorFlipParams flipParams[]
                           );
/**
 *******************************************************************************
 *
 * \brief Starts capture device on FFN TDA2x
 *
 * \param  captureSrc       [IN] Capture source params
 *
 * \param  captureOutWidth  [IN] Sensor capture width
 *
 * \param  captureOutHeight [IN] Sensor capture height
 *
 * \param  sensorId         [IN] Video sensor ID
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */
Int32 ChainsCommon_Amv_StartFFNCaptureDevice(Chains_CaptureSrc captureSrc,
                        UInt32 captureOutWidth,
                        UInt32 captureOutHeight,
                        Bsp_VidSensorConfigRegs *vidSensorConfigRegs,
                        UInt32 vidSensorConfigRegsSize,
                        AMV_VideoSensor_Id sensorId[]
                        );

/**
 *******************************************************************************
 *
 * \brief Stops capture device on FFN TDA2x
 *
 * \param  captureSrc     [IN] Capture source params
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */
Int32 ChainsCommon_Amv_StopFFNDevice(AMV_VideoSensor_Id sensorId[],
                                     UInt8 numLvdsCh);

/**
 *******************************************************************************
 *
 * \brief Stops capture device on FFN TDA2x
 *
 * \param  captureSrc     [IN] Capture source params
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */

Int32 ChainsCommon_Amv_StopFFNHdmiDevice(Chains_CaptureSrc captureSrc,
                                         AMV_VideoSensor_Id sensorId);

/**
 *******************************************************************************
 *
 * \brief Starts all cameras on FFN TDA2x
 *
 * \param  captureSrc       [IN] Capture source params
 *
 * \param  numLvdsCh        [IN] Number of channels
 *
 * \param  sensorId         [IN] Video sensor ID
 *
 *
 *******************************************************************************
 */
Int32 ChainsCommon_Amv_Cam_StartFFNCaptureDevice(Chains_CaptureSrc captureSrc,
                                                      UInt32 numLvdsCh,
                                                      AMV_VideoSensor_Id portId[],
                                                      Bsp_VidSensorConfigRegs **vidSensorConfigRegs,
                                                      UInt32 *vidSensorConfigRegsSizes,
                                                      Bsp_VidSensorFlipParams flipParams[]);

/**
 *******************************************************************************
 *
 * \brief Sets capture params for multi cam use case on TDA2x
 *
 * \param  pPrm             [IN] Capture link create params
 *
 * \param  captureInWidth   [IN] Capture input width
 *
 * \param  captureInHeight  [IN] Capture input height
 *
 * \param   portId          [IN] Ports used by this sensor,
 *
 * \param  numLvdsCh        [IN] Number of channels
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_Cam_SetCapturePrms(
                        CaptureLink_CreateParams *pPrm,
                        UInt32 captureInWidth,
                        UInt32 captureInHeight,
                        AMV_VideoSensor_Id portId[],
                        UInt32 numLvdsCh
                        );

/**
 *******************************************************************************
 *
 * \brief Returns video sensor create parameters
 *
 *******************************************************************************
 */
VidSensor_CreateParams *ChainsCommon_GetAmvSensorCreateParams();


/**
 *******************************************************************************
 *
 * \brief Configure and starts all devices on FFN TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_ConfigureFFNDevices();


/**
 *******************************************************************************
 *
 * \brief Switch active camera on FFN TDA2x. If 1st camera is active it will
 *        switch to 2nd (and opposite). If 3rd camera is active it will switch
 *        to 4th (and opposite).
 *
 *
 * \param  videoSensorPair       [IN] Group of video sensor devices
 *                                    Valid options are:
 *                                      FFN_VID_SENSOR_1_2
 *                                      FFN_VID_SENSOR_3_4
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SwitchFFNCamera(UInt8 videoSensorPair);

/**
 *******************************************************************************
 *
 * \brief   Set Capture Create Parameters for single FFN camera capture mode
 *
 * \param   pPrm         [IN]  CaptureLink_CreateParams
 *
 *******************************************************************************
*/
Void ChainsCommon_Amv_SingleCam_SetFFNCapturePrms(
                        CaptureLink_CreateParams *pPrm,
                        UInt32 captureInWidth,
                        UInt32 captureInHeight,
                        UInt32 captureOutWidth,
                        UInt32 captureOutHeight,
                        Chains_CaptureSrc captureSrc
                        );

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif

/* @} */
