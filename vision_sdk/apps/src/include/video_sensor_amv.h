/*
 *   Copyright (c) RT-RK     2017
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of RT-RK nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 *******************************************************************************
 *
 * \file video_sensor_amv.h
 *
 * \brief APIs for controlling external sensors.
 *
 *
 *******************************************************************************
 */

#ifndef _VIDEO_SENSOR_AMV_H_
#define _VIDEO_SENSOR_AMV_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include <include/link_api/system.h>
#include <ti/drv/vps/include/devices/bsp_videoSensor.h>
#include <ti/drv/vps/include/boards/bsp_board.h>
#include <ti/drv/vps/examples/utility/bsputils_lvds.h>
#include <ti/drv/vps/examples/utility/bsputils_ub960.h>
#include <src/rtos/iss/include/algorithmLink_issAewb.h>
#include <src/rtos/video_sensor/include/video_sensor.h>


/*******************************************************************************
 *  Defines
 *******************************************************************************
 */


/**
 *******************************************************************************
 *
 * \brief I2C address for 1st and 2nd video sensor and UB913
 *
 *******************************************************************************
 */
#define FFN_SENSOR_2_I2C_ADDR        (0x32)
#define FFN_SENSOR_3_I2C_ADDR        (0x33)
#define FFN_SENSOR_4_I2C_ADDR        (0x30)
#define FFN_SENSOR_1_I2C_ADDR        (0x31)

#define FFN_SENSOR_1_2_I2C_ADDR        (0x31)
#define FFN_SENSOR_3_4_I2C_ADDR        (0x30)


/**
 *******************************************************************************
 *
 * \brief I2C address for 3rd and 4th video sensor and UB913
 *
 *******************************************************************************
 */



/**
 *******************************************************************************
 *
 * \brief 1st and 2nd FFN video sensor
 *
 *******************************************************************************
 */
#define FFN_VID_SENSOR_1_2              (0)


/**
 *******************************************************************************
 *
 * \brief 3rd and 4th FFN video sensor
 *
 *******************************************************************************
 */
#define FFN_VID_SENSOR_3_4              (1)



/*******************************************************************************
 *  Enum's
 *******************************************************************************
 */


/**
  *******************************************************************************
 *
 * \brief Enum for all video sensor on AMV Board TDA2x
 *
 *******************************************************************************
 */
typedef enum
{
    FFN_VIDEO_SENSOR_1 = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTA,
    /**< 1st video sensor */

    FFN_VIDEO_SENSOR_2 = SYSTEM_CAPTURE_INST_VIP3_SLICE1_PORTA,
    /**< 2nd video sensor */

    FFN_VIDEO_SENSOR_3 = SYSTEM_CAPTURE_INST_VIP3_SLICE2_PORTA,
    /**< 3rd video sensor */

    FFN_VIDEO_SENSOR_4 = SYSTEM_CAPTURE_INST_VIP1_SLICE2_PORTA,
    /**< 4th video sensor */

    SCV_VIDEO_SENSOR_1_2 = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTB | 0x0100,
    SCV_VIDEO_SENSOR_3_4 = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTA | 0x0200,
    /**< Pair 1st and 2nd video sensors
     * 3rd and 4th paired automatically     */


    SCV_VIDEO_SENSOR_1_3 = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTB | 0x0300,
    SCV_VIDEO_SENSOR_2_4 = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTA | 0x0400,
    /**< Pair 1st and 3rd video sensors
     * 2nd and 4th paired automatically     */

    SCV_VIDEO_SENSOR_1_4 = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTB | 0x0500,
    SCV_VIDEO_SENSOR_2_3 = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTA | 0x0600,
    /**< Pair 1st and 4th video sensors
     * 2nd and 3rd paired automatically     */


    SCV_VIDEO_SENSOR_5_6 = SYSTEM_CAPTURE_INST_VIP3_SLICE2_PORTA,
    /**< 5th and 6th video sensor */

} AMV_VideoSensor_Id;

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */



/**
 *******************************************************************************
 *
 * \brief Create function to create video sensor.
 *
 *        Creates the sensor handle using bsp function calls.
 *
 * \param  createParams   [IN] Create parameters for Sensor
 * \param  createStatus   [OUT] Status
 * \param  sensorId       [IN] Video sensor ID
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
*/
Int32 VidSensorAmv_create(VidSensor_CreateParams *createParams,
                          VidSensor_CreateStatus *createStatus,
                          Bsp_VidSensorConfigRegs **vidSensorConfigRegs,
                          UInt32 *vidSensorConfigRegsSizes,
                          AMV_VideoSensor_Id sensorId[]);

/**
 *******************************************************************************
 *
 * \brief Delete function to delete video sensor.
 *
 *        Deletes the sensor handle using Fvid2_delete function calls.
 *
 * \param  handle         [IN] Handle to delete the sensor
 *
 * \param  deleteArgs      Not used.
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */
Int32 VidSensorAmv_delete(VidSensor_CreateParams *createParams,
                          Ptr deleteArgs);


/**
 *******************************************************************************
 *
 * \brief Control function to start stop and reset video sensor.
 *
 *        Control the sensor operation liske start and stop of the sensor using
 *        Fvid2 calls.
 *        Sensor reset is performed using IOCTL call IOCTL_BSP_VID_SENSOR_RESET
 *
 * \param  handle        [IN] Handle to control the sensor.
 *
 * \param  cmd           [IN] Control command for sensor
 *
 * \param  cmdArgs       [IN] Arguments for command if any.
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */
Int32 VidSensorAmv_control(VidSensor_CreateParams *createParams,
                           UInt32 cmd,
                           Ptr    cmdArgs,
                           UInt32 cmdStatusArgs);



/**
 *******************************************************************************
 *
 * \brief Flip image from video sensors
 *
 *******************************************************************************
 */
Int32 VidSensorAmv_flip(Bsp_VidSensorFlipParams flipParams[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif


/*@}*/
