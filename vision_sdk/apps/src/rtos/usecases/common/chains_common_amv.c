/*
 *   Copyright (c) RT-RK     2017
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of RT-RK nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *******************************************************************************
 *
 * \file chains_common_amv.c
 *
 * \brief This file contains common utility functions used by all AMV use-cases
 *
 *******************************************************************************
 */


/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include <src/include/chains.h>
#include <src/rtos/video_sensor/include/video_sensor.h>
#include <src/include/chains_common.h>
#include <src/include/chains_common_amv.h>
#include <src/include/video_sensor_amv.h>
/**
 *******************************************************************************
 *
 *  \brief  Common information related to all use-case
 *
 *******************************************************************************
*/

Chains_CommonObj gChains_commonAmvObj;

static Void ChainsCommon_SetVidSensorPrms(VidSensor_CreateParams *pPrm,
                                          VidSensor_Id sensorId,
                                          AMV_VideoSensor_Id portId[],
                                          UInt32 numCh,
                                          Chains_CaptureSrc captureSrc);

/**
 *******************************************************************************
 *
 * \brief Enables and starts all devices for CMS on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StartCMSDevices()
{
    Enable_HDMI();

#ifdef ALPHA_R003
    Power_Camera(AMV_SC_1ST_2ND_CAMERA, POWER_ON_CAM);
#else
    Power_Camera(AMV_SC_5TH_6TH_CAMERA, POWER_ON_CAM);
#endif

#ifndef A15_TARGET_OS_LINUX
    Power_UB964(AMV_DESER_UB964_2, POWER_ON_UB964);
#endif

    Task_sleep(UB964_POWER_ON_TIME_MS);

    Enable_OV490(AMV_OV490_3, PWDN_LOW_AMV_OV490, RESET_ON_AMV_OV490);

#ifndef A15_TARGET_OS_LINUX
    I2C_SwitchTo(I2C_SWITCH_CMS_ENABLE);
    I2C_SwitchTo(I2C_SWITCH_CH3);

    BspUtils_Ub964_config(AMV_DESER_UB964_2, ub964_config_CMS, ub964_config_CMS_numParams);
#endif

    Enable_OV490(AMV_OV490_3, PWDN_LOW_AMV_OV490, RESET_OFF_AMV_OV490);

    Task_sleep(1000); //wait OV490 to finish initialisation

#ifndef A15_TARGET_OS_LINUX
    I2C_SwitchTo(I2C_SWITCH_DISABLE);
#endif

    ChainsCommon_Amv_SetVipPinMux_3A();
}

/**
 *******************************************************************************
 *
 * \brief Disables and stops all devices for CMS on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StopCMSDevices()
{
#ifdef ALPHA_R003
    Power_Camera(AMV_SC_1ST_2ND_CAMERA, POWER_OFF_CAM);
#else
    Power_Camera(AMV_SC_5TH_6TH_CAMERA, POWER_OFF_CAM);
#endif

#ifndef A15_TARGET_OS_LINUX
    Power_UB964(AMV_DESER_UB964_2, POWER_OFF_UB964);
#endif

    Enable_OV490(AMV_OV490_3, PWDN_HIGH_AMV_OV490, RESET_ON_AMV_OV490);

}


/**
 *******************************************************************************
 *
 * \brief Enables and starts all devices for SV on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StartSVDevices()
{
    Enable_HDMI();

#ifdef ALPHA_R003
    Power_Camera(AMV_SC_5TH_6TH_CAMERA, POWER_ON_CAM);
#else
    Power_Camera(AMV_SC_1ST_2ND_CAMERA, POWER_ON_CAM);
#endif
    Power_Camera(AMV_SC_3D_4TH_CAMERA, POWER_ON_CAM);

#ifndef A15_TARGET_OS_LINUX
    Power_UB964(AMV_DESER_UB964_1, POWER_ON_UB964);
#endif

    Task_sleep(UB964_POWER_ON_TIME_MS);

    Enable_OV490(AMV_OV490_1, PWDN_LOW_AMV_OV490, RESET_ON_AMV_OV490);
    Enable_OV490(AMV_OV490_2, PWDN_LOW_AMV_OV490, RESET_ON_AMV_OV490);

#ifndef A15_TARGET_OS_LINUX
    I2C_SwitchTo(I2C_SWITCH_SV_ENABLE);
    I2C_SwitchTo(I2C_SWITCH_CH2);

    BspUtils_Ub964_config(AMV_DESER_UB964_1, ub964_config_SV, ub964_config_SV_numParams);

    I2C_SwitchTo(I2C_SWITCH_DISABLE);
#endif

    Enable_OV490(AMV_OV490_1, PWDN_LOW_AMV_OV490, RESET_OFF_AMV_OV490);

    Task_sleep(OV490_ENABLE_TIME_MS);

    Enable_OV490(AMV_OV490_2, PWDN_LOW_AMV_OV490, RESET_OFF_AMV_OV490);

    ChainsCommon_Amv_SetVipPinMux_1A();
    ChainsCommon_Amv_SetVipPinMux_1B();

}

/**
 *******************************************************************************
 *
 * \brief Disables and stops all devices for SV on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StopSVDevices()
{
#ifdef ALPHA_R003
    Power_Camera(AMV_SC_5TH_6TH_CAMERA, POWER_OFF_CAM);
#else
    Power_Camera(AMV_SC_1ST_2ND_CAMERA, POWER_OFF_CAM);
#endif
    Power_Camera(AMV_SC_3D_4TH_CAMERA, POWER_OFF_CAM);

#ifndef A15_TARGET_OS_LINUX
    Power_UB964(AMV_DESER_UB964_1, POWER_OFF_UB964);
#endif

    Enable_OV490(AMV_OV490_1, PWDN_HIGH_AMV_OV490, RESET_ON_AMV_OV490);
    Enable_OV490(AMV_OV490_2, PWDN_HIGH_AMV_OV490, RESET_ON_AMV_OV490);
}


/**
 *******************************************************************************
 *
 * \brief Enables and starts all devices on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StartSCDevices()
{
    Enable_HDMI();

    Power_Camera(AMV_SC_1ST_2ND_CAMERA, POWER_ON_CAM);
    Task_sleep(UB964_POWER_ON_TIME_MS);

    Power_Camera(AMV_SC_3D_4TH_CAMERA, POWER_ON_CAM);
    Task_sleep(UB964_POWER_ON_TIME_MS);

    Power_Camera(AMV_SC_5TH_6TH_CAMERA, POWER_ON_CAM);
    Task_sleep(UB964_POWER_ON_TIME_MS);

#ifndef A15_TARGET_OS_LINUX
    Power_UB964(AMV_DESER_UB964_1, POWER_ON_UB964);
    Task_sleep(UB964_POWER_ON_TIME_MS);
    Power_UB964(AMV_DESER_UB964_2, POWER_ON_UB964);
    Task_sleep(UB964_POWER_ON_TIME_MS);
#endif

    Enable_OV490(AMV_OV490_1, PWDN_LOW_AMV_OV490, RESET_ON_AMV_OV490);
    Enable_OV490(AMV_OV490_2, PWDN_LOW_AMV_OV490, RESET_ON_AMV_OV490);
    Enable_OV490(AMV_OV490_3, PWDN_LOW_AMV_OV490, RESET_ON_AMV_OV490);

#ifndef A15_TARGET_OS_LINUX
    I2C_SwitchTo(I2C_SWITCH_CH2);

    BspUtils_Ub964_config(AMV_DESER_UB964_1, ub964_config_6cam_SV, ub964_config_6cam_SV_numParams);

    I2C_SwitchTo(I2C_SWITCH_DISABLE);
    I2C_SwitchTo(I2C_SWITCH_CH3);

    BspUtils_Ub964_config(AMV_DESER_UB964_2, ub964_config_6cam_CMS, ub964_config_6cam_CMS_numParams);

    I2C_SwitchTo(I2C_SWITCH_DISABLE);
#endif

    Enable_OV490(AMV_OV490_1, PWDN_LOW_AMV_OV490, RESET_OFF_AMV_OV490);
    Task_sleep(OV490_ENABLE_TIME_MS);
    Enable_OV490(AMV_OV490_2, PWDN_LOW_AMV_OV490, RESET_OFF_AMV_OV490);
    Task_sleep(OV490_ENABLE_TIME_MS);
    Enable_OV490(AMV_OV490_3, PWDN_LOW_AMV_OV490, RESET_OFF_AMV_OV490);

    ChainsCommon_Amv_SetVipPinMux_1A();
    ChainsCommon_Amv_SetVipPinMux_1B();
    ChainsCommon_Amv_SetVipPinMux_3A();

}


/**
 *******************************************************************************
 *
 * \brief Disables and stops all devices on SC TDA2x
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_StopSCDevices()
{
    Power_Camera(AMV_SC_1ST_2ND_CAMERA, POWER_OFF_CAM);
    Power_Camera(AMV_SC_3D_4TH_CAMERA, POWER_OFF_CAM);
    Power_Camera(AMV_SC_5TH_6TH_CAMERA, POWER_OFF_CAM);

#ifndef A15_TARGET_OS_LINUX
    Power_UB964(AMV_DESER_UB964_1, POWER_OFF_UB964);
    Power_UB964(AMV_DESER_UB964_2, POWER_OFF_UB964);
#endif

    Enable_OV490(AMV_OV490_1, PWDN_HIGH_AMV_OV490, RESET_ON_AMV_OV490);
    Enable_OV490(AMV_OV490_2, PWDN_HIGH_AMV_OV490, RESET_ON_AMV_OV490);
    Enable_OV490(AMV_OV490_3, PWDN_HIGH_AMV_OV490, RESET_ON_AMV_OV490);

}


/**
 *******************************************************************************
 *
 * \brief Switch active camera on FFN TDA2x. If 1st camera is active it will
 *        switch to 2nd (and opposite). If 3rd camera is active it will switch
 *        to 4th (and opposite).
 *
 *
 * \param  videoSensorPair       [IN] Group of video sensor devices
 *                                    Valid options are:
 *                                      FFN_VID_SENSOR_1_2
 *                                      FFN_VID_SENSOR_3_4
 *
 *******************************************************************************
 */
#ifndef ALPHA_R003
Void ChainsCommon_Amv_SwitchFFNCamera(UInt8 videoSensorPair)
{
    static UInt8 flag1 = 1;
    static UInt8 flag2 = 1;

    if (FFN_VID_SENSOR_1_2 == videoSensorPair)
    {
        if (flag1)
        {
            Mux_Channels(AMV_DESER_UB914_2, 0);
            flag1 = 0;
        }
        else
        {
            Mux_Channels(AMV_DESER_UB914_2, 1);
            flag1 = 1;
        }
    }
    else if (FFN_VID_SENSOR_3_4 == videoSensorPair)
    {
        if (flag2)
        {
            Mux_Channels(AMV_DESER_UB914_1, 0);
            flag2 = 0;
        }
        else
        {
          Mux_Channels(AMV_DESER_UB914_1, 1);
          flag2 = 1;
        }
    }

}
#endif

/**
 *******************************************************************************
 *
 * \brief Configure and starts devices on FFN TDA2x
 *
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_ConfigureFFNDevices()
{
    init_desers();
}
/**
 *******************************************************************************
 *
 * \brief Configure and starts devices on FFN TDA2x
 *
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_ConfigureFFNDevice(AMV_VideoSensor_Id sensorId)
{
    static Bool firstTime = TRUE;

    if (firstTime)
    {
        init_desers();
        Enable_HDMI();
        firstTime = FALSE;
     }
#ifndef A15_TARGET_OS_LINUX
#ifdef ALPHA_R003

    switch(sensorId)
    {
        case FFN_VIDEO_SENSOR_1:
          Power_Camera(AMV_FFN_7TH_8TH_CAMERA, POWER_ON_CAM);
          Task_sleep(CAMERA_POWER_ON_TIME_MS);

          BspUtils_Ub914_config(AMV_DESER_UB914_2, ub914_config_deser2, ub914_config_numParams_deser2);

          BspUtils_Ub913_config(AMV_SER_UB913_2, ub913_config_ser, ub913_config_numParams);

          ChainsCommon_Amv_SetVipPinMux_1A();

        break;

        case FFN_VIDEO_SENSOR_2:
          Power_Camera(AMV_FFN_7TH_8TH_CAMERA, POWER_ON_CAM);
          Task_sleep(CAMERA_POWER_ON_TIME_MS);

          BspUtils_Ub914_config(AMV_DESER_UB914_3, ub914_config_deser3, ub914_config_numParams_deser3);

          BspUtils_Ub913_config(AMV_SER_UB913_3, ub913_config_ser, ub913_config_numParams);

          ChainsCommon_Amv_SetVipPinMux_5A();

        break;

        case FFN_VIDEO_SENSOR_3:
          Power_Camera(AMV_FFN_9TH_10TH_CAMERA, POWER_ON_CAM);
          Task_sleep(CAMERA_POWER_ON_TIME_MS);

          BspUtils_Ub914_config(AMV_DESER_UB914_4, ub914_config_deser4, ub914_config_numParams_deser4);
          BspUtils_Ub913_config(AMV_SER_UB913_4, ub913_config_ser, ub913_config_numParams);

          ChainsCommon_Amv_SetVipPinMux_6A();

        break;

        case FFN_VIDEO_SENSOR_4:
          Power_Camera(AMV_FFN_9TH_10TH_CAMERA, POWER_ON_CAM);
          Task_sleep(CAMERA_POWER_ON_TIME_MS+1000);

          BspUtils_Ub914_config(AMV_DESER_UB914_1, ub914_config_deser1, ub914_config_numParams_deser1);

          BspUtils_Ub913_config(AMV_SER_UB913_1, ub913_config_ser, ub913_config_numParams);

          ChainsCommon_Amv_SetVipPinMux_2A();

        break;

    }
#else

    Power_Camera(AMV_FFN_1ST_3D_CAMERA, POWER_ON_CAM);
    Task_sleep(CAMERA_POWER_ON_TIME_MS);
    Power_Camera(AMV_FFN_2ND_CAMERA, POWER_ON_CAM);
    Task_sleep(CAMERA_POWER_ON_TIME_MS);
    Power_Camera(AMV_FFN_4TH_CAMERA, POWER_ON_CAM);
    Task_sleep(CAMERA_POWER_ON_TIME_MS);
    /* Power on UB914 */
    Power_UB914(AMV_DESER_UB914_2, POWER_ON_UB914);
    Task_sleep(UB914_POWER_ON_TIME_MS);
    /* Configure UB914 and UB913 */
    BspUtils_Ub914_config(AMV_DESER_UB914_2, ub914_config_deser2, ub914_config_numParams_deser2);

    Mux_Channels(AMV_DESER_UB914_2, 0);
    BspUtils_Ub913_config(AMV_SER_UB913_34, ub913_config_ser, ub913_config_numParams);
    Mux_Channels(AMV_DESER_UB914_2, 1);
    BspUtils_Ub913_config(AMV_SER_UB913_34, ub913_config_ser, ub913_config_numParams);

    /* Power on UB914 */
    Power_UB914(AMV_DESER_UB914_1, POWER_ON_UB914);
    Task_sleep(UB914_POWER_ON_TIME_MS);
    /* Configure UB914 and UB913 */
    BspUtils_Ub914_config(AMV_DESER_UB914_1, ub914_config_deser1, ub914_config_numParams_deser1);

    Mux_Channels(AMV_DESER_UB914_1, 0);
    BspUtils_Ub913_config(AMV_SER_UB913_12, ub913_config_ser, ub913_config_numParams);
    Mux_Channels(AMV_DESER_UB914_1, 1);
    BspUtils_Ub913_config(AMV_SER_UB913_12, ub913_config_ser, ub913_config_numParams);
#endif
#endif
}


/**
 *******************************************************************************
 *
 * \brief Deconfigure and stops devices on FFN TDA2x
 *
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_DeconfigureFFNDevice(AMV_VideoSensor_Id sensorId)
{

    switch(sensorId)
    {
        case FFN_VIDEO_SENSOR_1:
#ifdef ALPHA_R003
            Power_Camera(AMV_FFN_7TH_8TH_CAMERA, POWER_OFF_CAM);
#else
#ifndef A15_TARGET_OS_LINUX
            Power_UB914(AMV_DESER_UB914_2, POWER_OFF_UB914);
#endif
#endif
        break;

        case FFN_VIDEO_SENSOR_2:
#ifdef ALPHA_R003
            Power_Camera(AMV_FFN_7TH_8TH_CAMERA, POWER_OFF_CAM);
#else
#ifndef A15_TARGET_OS_LINUX
            Power_UB914(AMV_DESER_UB914_3, POWER_OFF_UB914);
#endif
#endif
        break;

        case FFN_VIDEO_SENSOR_3:
#ifdef ALPHA_R003
            Power_Camera(AMV_FFN_9TH_10TH_CAMERA, POWER_OFF_CAM);
#else
#ifndef A15_TARGET_OS_LINUX
            Power_UB914(AMV_DESER_UB914_4, POWER_OFF_UB914);
#endif
#endif
        break;

        case FFN_VIDEO_SENSOR_4:
#ifdef ALPHA_R003
            Power_Camera(AMV_FFN_9TH_10TH_CAMERA, POWER_OFF_CAM);
#else
#ifndef A15_TARGET_OS_LINUX
            Power_UB914(AMV_DESER_UB914_1, POWER_OFF_UB914);
#endif
#endif
        break;
    }

}


/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 1A port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_1A()
{
    UInt32 isLocked[2];

    /* MMR_LOCK_5 */
    if (0x6F361E05 == *(volatile UInt32 *)(0x4A002550))
    {
        isLocked[0] = 0;
    }
    else
    {
        isLocked[0] = 1;
        *(volatile UInt32 *)(0x4A002550) = 0x6F361E05;
    }
    /* MMR_LOCK_1 */
    if (0x2FF1AC2B == *(volatile UInt32 *)(0x4A002540))
    {
        isLocked[1] = 0;
    }
    else
    {
        isLocked[1] = 1;
        *(volatile UInt32 *)(0x4A002540) = 0x2FF1AC2B;
    }

    /* VIN1A PCLK/H/V */
    *(volatile UInt32 *)(0x4A0034DC) = 0x40000;
    *(volatile UInt32 *)(0x4A0034EC) = 0x40000;
    *(volatile UInt32 *)(0x4A0034F0) = 0x40000;

    /* VIN1A Dx */
    *(volatile UInt32 *)(0x4A0034F4) = 0x40000;
    *(volatile UInt32 *)(0x4A0034F8) = 0x40000;
    *(volatile UInt32 *)(0x4A0034FC) = 0x40000;
    *(volatile UInt32 *)(0x4A003500) = 0x40000;
    *(volatile UInt32 *)(0x4A003504) = 0x40000;
    *(volatile UInt32 *)(0x4A003508) = 0x40000;
    *(volatile UInt32 *)(0x4A00350C) = 0x40000;
    *(volatile UInt32 *)(0x4A003510) = 0x40000;

    *(volatile UInt32 *)(0x4A002534) |= 0x000;

    if(1 == isLocked[0])
    {
        /* MMR_LOCK_5 */
        *(volatile UInt32 *)(0x4A002550) = 0x143F832C;
    }
    if(1 == isLocked[1])
    {
        /* MMR_LOCK_1 */
        *(volatile UInt32 *)(0x4A002540) = 0x1A1C8144;
    }

}


/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 1B port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_1B()
{
    UInt32 isLocked[2];

    /* MMR_LOCK_5 */
    if (0x6F361E05 == *(volatile UInt32 *)(0x4A002550))
    {
        isLocked[0] = 0;
    }
    else
    {
        isLocked[0] = 1;
        *(volatile UInt32 *)(0x4A002550) = 0x6F361E05;
    }
    /* MMR_LOCK_1 */
    if (0x2FF1AC2B == *(volatile UInt32 *)(0x4A002540))
    {
        isLocked[1] = 0;
    }
    else
    {
        isLocked[1] = 1;
        *(volatile UInt32 *)(0x4A002540) = 0x2FF1AC2B;
    }

    /* VIN1B PCLK/H/V */
    *(volatile UInt32 *)(0x4A0034E0) = 0xC0000;
    *(volatile UInt32 *)(0x4A0034E4) = 0x40001;
    *(volatile UInt32 *)(0x4A0034E8) = 0x40001;

    /* VIN1B Dx */
    *(volatile UInt32 *)(0x4A003514) = 0x40001;
    *(volatile UInt32 *)(0x4A003518) = 0x40001;
    *(volatile UInt32 *)(0x4A00351C) = 0x40001;
    *(volatile UInt32 *)(0x4A003520) = 0x40001;
    *(volatile UInt32 *)(0x4A003524) = 0x40001;
    *(volatile UInt32 *)(0x4A003528) = 0x40001;
    *(volatile UInt32 *)(0x4A00352C) = 0x40001;
    *(volatile UInt32 *)(0x4A003530) = 0x40001;

    *(volatile UInt32 *)(0x4A002534) |= 0x000;

    if(1 == isLocked[0])
    {
        /* MMR_LOCK_5 */
        *(volatile UInt32 *)(0x4A002550) = 0x143F832C;
    }
    if(1 == isLocked[1])
    {
        /* MMR_LOCK_1 */
        *(volatile UInt32 *)(0x4A002540) = 0x1A1C8144;
    }
}


/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 2A port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_2A()
{
    UInt32 isLocked[2];

    /* MMR_LOCK_5 */
    if (0x6F361E05 == *(volatile UInt32 *)(0x4A002550))
    {
        isLocked[0] = 0;
    }
    else
    {
        isLocked[0] = 1;
        *(volatile UInt32 *)(0x4A002550) = 0x6F361E05;
    }
    /* MMR_LOCK_1 */
    if (0x2FF1AC2B == *(volatile UInt32 *)(0x4A002540))
    {
        isLocked[1] = 0;
    }
    else
    {
        isLocked[1] = 1;
        *(volatile UInt32 *)(0x4A002540) = 0x2FF1AC2B;
    }

    //VIN2A PCLK/H/V
    *(volatile UInt32 *)(0x4A003554) = 0x40000;
    *(volatile UInt32 *)(0x4A003560) = 0x40000;
    *(volatile UInt32 *)(0x4A003564) = 0x40000;
    //VIN2A Dx
    *(volatile UInt32 *)(0x4A003568) = 0x40000;
    *(volatile UInt32 *)(0x4A00356C) = 0x40000;
    *(volatile UInt32 *)(0x4A003570) = 0x40000;
    *(volatile UInt32 *)(0x4A003574) = 0x40000;
    *(volatile UInt32 *)(0x4A003578) = 0x40000;
    *(volatile UInt32 *)(0x4A00357C) = 0x40000;
    *(volatile UInt32 *)(0x4A003580) = 0x40000;
    *(volatile UInt32 *)(0x4A003584) = 0x40000;

    *(volatile UInt32 *)(0x4A002534) |= 0x000;

    if(1 == isLocked[0])
    {
        /* MMR_LOCK_5 */
        *(volatile UInt32 *)(0x4A002550) = 0x143F832C;
    }
    if(1 == isLocked[1])
    {
        /* MMR_LOCK_1 */
        *(volatile UInt32 *)(0x4A002540) = 0x1A1C8144;
    }

}


/**
 *******************************************************************************
 *
 * \brief Sets VIP pinmux for 3A port
 *
 *******************************************************************************
 */
Void ChainsCommon_Amv_SetVipPinMux_3A()
{
    UInt32 isLocked[2];

    /* MMR_LOCK_5 */
    if (0x6F361E05 == *(volatile UInt32 *)(0x4A002550))
    {
        isLocked[0] = 0;
    }
    else
    {
        isLocked[0] = 1;
        *(volatile UInt32 *)(0x4A002550) = 0x6F361E05;
    }
    /* MMR_LOCK_1 */
    if (0x2FF1AC2B == *(volatile UInt32 *)(0x4A002540))
    {
        isLocked[1] = 0;
    }
    else
    {
        isLocked[1] = 1;
        *(volatile UInt32 *)(0x4A002540) = 0x2FF1AC2B;
    }


        //VIN3A PCLK/H/V
        *(volatile UInt32 *)(0x4A003698) = 0x40007;
        *(volatile UInt32 *)(0x4A0036B8) = 0x40007;
        *(volatile UInt32 *)(0x4A0036B4) = 0x40007;
        //VIN3A Dx
        *(volatile UInt32 *)(0x4A003730) = 0x40007;
        *(volatile UInt32 *)(0x4A00372C) = 0x40007;
        *(volatile UInt32 *)(0x4A003728) = 0x40007;
        *(volatile UInt32 *)(0x4A003724) = 0x40007;
        *(volatile UInt32 *)(0x4A003710) = 0x40007;
        *(volatile UInt32 *)(0x4A00370C) = 0x40007;
        *(volatile UInt32 *)(0x4A0036F8) = 0x40007;
        *(volatile UInt32 *)(0x4A0036F4) = 0x40007;

        *(volatile UInt32 *)(0x4A002534) |= 0x000;

        if(1 == isLocked[0])
        {
            /* MMR_LOCK_5 */
            *(volatile UInt32 *)(0x4A002550) = 0x143F832C;
        }
        if(1 == isLocked[1])
        {
            /* MMR_LOCK_1 */
            *(volatile UInt32 *)(0x4A002540) = 0x1A1C8144;
        }
}



Void ChainsCommon_Amv_SetVipPinMux_5A()
{
    UInt32 isLocked[2];

    /* MMR_LOCK_5 */
    if (0x6F361E05 == *(volatile UInt32 *)(0x4A002550))
    {
        isLocked[0] = 0;
    }
    else
    {
        isLocked[0] = 1;
        *(volatile UInt32 *)(0x4A002550) = 0x6F361E05;
    }
    /* MMR_LOCK_1 */
    if (0x2FF1AC2B == *(volatile UInt32 *)(0x4A002540))
    {
        isLocked[1] = 0;
    }
    else
    {
        isLocked[1] = 1;
        *(volatile UInt32 *)(0x4A002540) = 0x2FF1AC2B;
    }

    //VIN5A PCLK/H/V
    *(volatile UInt32 *)(0x4A003774) = 0x40009;
    *(volatile UInt32 *)(0x4A00379C) = 0x40009;
    *(volatile UInt32 *)(0x4A0037A0) = 0x40009;
    //VIN5A Dx
    *(volatile UInt32 *)(0x4A003798) = 0x40009; //D0
    *(volatile UInt32 *)(0x4A003794) = 0x40009; //D1
    *(volatile UInt32 *)(0x4A003790) = 0x40009; //D2
    *(volatile UInt32 *)(0x4A00378C) = 0x40009; //D3
    *(volatile UInt32 *)(0x4A003788) = 0x40009; //D4
    *(volatile UInt32 *)(0x4A003784) = 0x40009; //D5
    *(volatile UInt32 *)(0x4A003780) = 0x40009; //D6
    *(volatile UInt32 *)(0x4A00377C) = 0x40009; //D7

    *(volatile UInt32 *)(0x4A002534) |= 0x000;

    if(1 == isLocked[0])
    {
        /* MMR_LOCK_5 */
        *(volatile UInt32 *)(0x4A002550) = 0x143F832C;
    }
    if(1 == isLocked[1])
    {
        /* MMR_LOCK_1 */
        *(volatile UInt32 *)(0x4A002540) = 0x1A1C8144;
    }

}


Void ChainsCommon_Amv_SetVipPinMux_6A()
{
    UInt32 isLocked[2];

    /* MMR_LOCK_5 */
    if (0x6F361E05 == *(volatile UInt32 *)(0x4A002550))
    {
        isLocked[0] = 0;
    }
    else
    {
        isLocked[0] = 1;
        *(volatile UInt32 *)(0x4A002550) = 0x6F361E05;
    }
    /* MMR_LOCK_1 */
    if (0x2FF1AC2B == *(volatile UInt32 *)(0x4A002540))
    {
        isLocked[1] = 0;
    }
    else
    {
        isLocked[1] = 1;
        *(volatile UInt32 *)(0x4A002540) = 0x2FF1AC2B;
    }

    //VIN5A PCLK/H/V
    *(volatile UInt32 *)(0x4A003698) = 0x40007;
    *(volatile UInt32 *)(0x4A0036B8) = 0x40007;
    *(volatile UInt32 *)(0x4A0036B4) = 0x40007;
    //VIN5A Dx
    *(volatile UInt32 *)(0x4A003730) = 0x40007; //D0
    *(volatile UInt32 *)(0x4A00372C) = 0x40007; //D1
    *(volatile UInt32 *)(0x4A003728) = 0x40007; //D2
    *(volatile UInt32 *)(0x4A003724) = 0x40007; //D3
    *(volatile UInt32 *)(0x4A003710) = 0x40007; //D4
    *(volatile UInt32 *)(0x4A00370C) = 0x40007; //D5
    *(volatile UInt32 *)(0x4A0036F8) = 0x40007; //D6
    *(volatile UInt32 *)(0x4A0036F4) = 0x40007; //D7

    *(volatile UInt32 *)(0x4A002534) |= 0x000;

    if(1 == isLocked[0])
    {
        /* MMR_LOCK_5 */
        *(volatile UInt32 *)(0x4A002550) = 0x143F832C;
    }
    if(1 == isLocked[1])
    {
        /* MMR_LOCK_1 */
        *(volatile UInt32 *)(0x4A002540) = 0x1A1C8144;
    }

}


/**
 *******************************************************************************
 *
 * \brief Starts all cameras on FFN TDA2x
 *
 * \param  captureSrc       [IN] Capture source params
 *
 * \param  numLvdsCh        [IN] Number of channels
 *
 * \param  sensorId         [IN] Video sensor ID
 *
 *
 *******************************************************************************
 */
Int32 ChainsCommon_Amv_Cam_StartFFNCaptureDevice(Chains_CaptureSrc captureSrc,
                                                 UInt32 numLvdsCh,
                                                 AMV_VideoSensor_Id portId[],
                                                 Bsp_VidSensorConfigRegs **vidSensorConfigRegs,
                                                 UInt32 *vidSensorConfigRegsSizes,
                                                 Bsp_VidSensorFlipParams flipParams[])
{
    Int32 status = SYSTEM_LINK_STATUS_SOK;
    UInt8 i;
    VidSensor_CreateStatus vidSensorStatus;

    Vps_printf(" CHAINS: Sensor create in progress !!!\n");

    init_desers();
    for(i = 0; i < numLvdsCh; i++)
    {
        ChainsCommon_Amv_ConfigureFFNDevice(portId[i]);
    }

    UTILS_assert(captureSrc==CHAINS_CAPTURE_SRC_OV10635);

    ChainsCommon_SetVidSensorPrms(
            &gChains_commonAmvObj.vidSensorPrm,
            VID_SENSOR_OV10635,
            NULL,
            numLvdsCh,
            captureSrc
            );

    status = VidSensorAmv_create(&gChains_commonAmvObj.vidSensorPrm,
                                &vidSensorStatus,
                                vidSensorConfigRegs,
                                vidSensorConfigRegsSizes,
                                portId);

    UTILS_assert(status == SYSTEM_LINK_STATUS_SOK);

    VidSensorAmv_flip(flipParams);

    VidSensorAmv_control(&gChains_commonAmvObj.vidSensorPrm,
                                VID_SENSOR_CMD_START,
                                NULL,
                                NULL);

    Vps_printf(" CHAINS: Sensor create ... DONE !!!\n");

    return status;
}

/**
 *******************************************************************************
 *
 * \brief Stops capture device on FFN TDA2x
 *
 * \param  captureSrc     [IN] Capture source params
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */
Int32 ChainsCommon_Amv_StopFFNDevice(AMV_VideoSensor_Id sensorId[],
                                     UInt8 numLvdsCh)
{
    Int32 status=0;
    UInt8 i;

    VidSensorAmv_control(&gChains_commonAmvObj.vidSensorPrm,
                              VID_SENSOR_CMD_STOP,
                              NULL,
                              NULL);

    VidSensorAmv_delete(&gChains_commonAmvObj.vidSensorPrm, NULL);


    for(i = 0; i < numLvdsCh; i++)
    {
        ChainsCommon_Amv_DeconfigureFFNDevice(sensorId[i]);
    }

    return status;
}

/**
 *******************************************************************************
 *
 * \brief Stops capture device on FFN TDA2x
 *
 * \param  captureSrc     [IN] Capture source params
 *
 * \param  sensorId       [IN] Video sensor ID
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
 */
Int32 ChainsCommon_Amv_StopFFNHdmiDevice(Chains_CaptureSrc captureSrc,
                                            AMV_VideoSensor_Id sensorId)
{
    Int32 status=0;

    if((captureSrc == CHAINS_CAPTURE_SRC_HDMI_720P) ||
        (captureSrc == CHAINS_CAPTURE_SRC_HDMI_1080P))
    {
        status = HdmiRecvr_control(&gChains_commonAmvObj.hdmiRecvrPrm,
                                    HDMI_RECVR_CMD_STOP,
                                    NULL,
                                    NULL);

        UTILS_assert(status == SYSTEM_LINK_STATUS_SOK);

        HdmiRecvr_delete(&gChains_commonAmvObj.hdmiRecvrPrm, NULL);
    }

    ChainsCommon_Amv_DeconfigureFFNDevice(sensorId);

    return status;
}

/**
 *******************************************************************************
 *
 * \brief   Set Sensor Create Parameters
 *
 *          This function is used to set the Video Sensor params.
 *          It is called in Create function. It is advisable to have
 *          Chains_VipSingleCameraView_ResetLinkPrms prior to set params
 *          so all the default params get set.
 *          Video sensor Id , Instance Id data format are set.
 *
 * \param   pPrm      [IN]    VidSensor_CreateParams
 * \param   sensorId  [IN]    Sensor ID
 * \param   portId    [OUT]   Ports used by this sensor,
 *                            only valid when sensorId is
 *                            VID_SENSOR_MULDES_OV1063X
 * \param   numCh     [IN]    Num of channels used
 *                            only valid when sensorId is
 *                            VID_SENSOR_MULDES_OV1063X or captureSrc is
 *                            CHAINS_CAPTURE_SRC_UB960_TIDA00262
 *
 *******************************************************************************
 */
static Void ChainsCommon_SetVidSensorPrms(
                VidSensor_CreateParams *pPrm,
                VidSensor_Id sensorId,
                AMV_VideoSensor_Id portId[],
                UInt32 numCh,
                Chains_CaptureSrc captureSrc
                )
{
    if(sensorId==VID_SENSOR_OV10635)
    {
        pPrm->sensorId        = VID_SENSOR_OV10635;
        pPrm->standard        = SYSTEM_STD_720P_60;
        pPrm->dataformat      = SYSTEM_DF_YUV422I_UYVY;
        pPrm->videoIfWidth    = SYSTEM_VIFW_8BIT;
        pPrm->fps             = SYSTEM_FPS_30;
        pPrm->isLVDSCaptMode  = FALSE;
        pPrm->numChan         = numCh;
    }

}

/**
 *******************************************************************************
 *
 * \brief Returns video sensor create parameters
 *
 *******************************************************************************
 */
VidSensor_CreateParams *ChainsCommon_GetAmvSensorCreateParams()
{
    return (&gChains_commonAmvObj.vidSensorPrm);
}

/**
 *******************************************************************************
 *
 * \brief   Set Capture Create Parameters for multi camera capture mode
 *
 * \param   pPrm         [IN]  CaptureLink_CreateParams
 *
 *******************************************************************************
*/
Void ChainsCommon_Amv_Cam_SetCapturePrms(
                        CaptureLink_CreateParams *pPrm,
                        UInt32 captureInWidth,
                        UInt32 captureInHeight,
                        AMV_VideoSensor_Id portId[],
                        UInt32 numLvdsCh
                        )
{
    UInt32 i, streamId;

    CaptureLink_VipInstParams *pInstPrm;
    CaptureLink_InParams *pInprms;
    CaptureLink_OutParams *pOutprms;
    CaptureLink_VipScParams *pScPrm;
    CaptureLink_VipPortConfig    *pPortCfg;

    memset(pPrm, 0, sizeof(*pPrm));

    pPrm->numVipInst = numLvdsCh;
    pPrm->numDssWbInst = 0;

    for (i=0; i<numLvdsCh; i++)
    {
        pInstPrm = &pPrm->vipInst[i];

        UTILS_assert(portId != NULL);
        pInstPrm->vipInstId     =   portId[i] & 0xFF; //AMV_VIDEO_PORT_MSK;


        pInstPrm->videoIfMode   =   SYSTEM_VIFM_SCH_DS_HSYNC_VSYNC;
        pInstPrm->videoIfWidth  =   SYSTEM_VIFW_8BIT;
        pInstPrm->bufCaptMode   =   SYSTEM_CAPT_BCM_FRM_DROP;
        pInstPrm->numStream     =   1;

        pInprms = &pInstPrm->inParams;

        pInprms->width      =   captureInWidth;
        pInprms->height     =   captureInHeight;
        pInprms->dataFormat =   SYSTEM_DF_YUV422P;
        pInprms->scanFormat =   SYSTEM_SF_PROGRESSIVE;

        for (streamId = 0; streamId < CAPTURE_LINK_MAX_OUTPUT_PER_INST;
                streamId++)
        {
            pOutprms = &pInstPrm->outParams[streamId];
            pOutprms->width         =   pInprms->width;
            pOutprms->height        =   pInprms->height;
            pOutprms->dataFormat    =   SYSTEM_DF_YUV420SP_UV;
            pOutprms->maxWidth      =   pOutprms->width;
            pOutprms->maxHeight     =   pOutprms->height;
            pOutprms->scEnable      =   FALSE;

            /* sub-frame not supported, set to FALSE */
            pOutprms->subFrmPrms.subFrameEnable = FALSE;
            pOutprms->subFrmPrms.numLinesPerSubFrame = 0;

        }
        pScPrm = &pInstPrm->scPrms;
        pScPrm->inCropCfg.cropStartX = 0;
        pScPrm->inCropCfg.cropStartY = 0;
        pScPrm->inCropCfg.cropWidth = pInprms->width;
        pScPrm->inCropCfg.cropHeight = pInprms->height;

        pScPrm->scCfg.bypass       = FALSE;
        pScPrm->scCfg.nonLinear    = FALSE;
        pScPrm->scCfg.stripSize    = 0;

        pScPrm->userCoeff = FALSE;

        /* pScPrm->scCoeffCfg is not reuquired in case
         * pScPrm->userCoeff is FALSE
         */
        pPortCfg = &pInstPrm->vipPortCfg;
        pPortCfg->syncType          =   SYSTEM_VIP_SYNC_TYPE_DIS_SINGLE_YUV;
        pPortCfg->ancCropEnable     =   FALSE;

        pPortCfg->intfCfg.clipActive    =   FALSE;
        pPortCfg->intfCfg.clipBlank     =   FALSE;
        pPortCfg->intfCfg.intfWidth     =   SYSTEM_VIFW_16BIT;

        pPortCfg->disCfg.fidSkewPostCnt     =   0;
        pPortCfg->disCfg.fidSkewPreCnt      =   0;
        pPortCfg->disCfg.lineCaptureStyle   =
                                SYSTEM_VIP_LINE_CAPTURE_STYLE_ACTVID;
        pPortCfg->disCfg.fidDetectMode      =   SYSTEM_VIP_FID_DETECT_MODE_PIN;
        pPortCfg->disCfg.actvidPol          =   SYSTEM_POL_HIGH;
        pPortCfg->disCfg.vsyncPol           =   SYSTEM_POL_HIGH;
        pPortCfg->disCfg.hsyncPol           =   SYSTEM_POL_HIGH;
        pPortCfg->disCfg.discreteBasicMode  =   TRUE;

        pPortCfg->comCfg.ctrlChanSel        =   SYSTEM_VIP_CTRL_CHAN_SEL_7_0;
        pPortCfg->comCfg.ancChSel8b         =
                            SYSTEM_VIP_ANC_CH_SEL_8B_LUMA_SIDE;
        pPortCfg->comCfg.pixClkEdgePol      =   SYSTEM_EDGE_POL_RISING;
        pPortCfg->comCfg.invertFidPol       =   FALSE;
        pPortCfg->comCfg.enablePort         =   FALSE;
        pPortCfg->comCfg.expectedNumLines   =   pInprms->height;
        pPortCfg->comCfg.expectedNumPix     =   pInprms->width;
        pPortCfg->comCfg.repackerMode       =   SYSTEM_VIP_REPACK_CBA_TO_CBA;

        pPortCfg->actCropEnable                         =   TRUE;
        pPortCfg->actCropCfg.srcNum                     =   0;
        pPortCfg->actCropCfg.cropCfg.cropStartX         =   0;
        pPortCfg->actCropCfg.cropCfg.cropStartY         =   0;
        pPortCfg->actCropCfg.cropCfg.cropWidth          =   pInprms->width;
        pPortCfg->actCropCfg.cropCfg.cropHeight         =   pInprms->height;

        pPortCfg->ancCropCfg.srcNum                     =   0;
        pPortCfg->ancCropCfg.cropCfg.cropStartX         =   0;
        pPortCfg->ancCropCfg.cropCfg.cropStartY         =   0;
        pPortCfg->ancCropCfg.cropCfg.cropWidth          =   0;
        pPortCfg->ancCropCfg.cropCfg.cropHeight         =   0;

        pInstPrm->numBufs = CAPTURE_LINK_NUM_BUFS_PER_CH_DEFAULT + 2;
    }
}

Void ChainsCommon_Amv_Start_Camera(
                           Amv_ChipType chipType,
                           Chains_CaptureSrc pCaptureSrc,
                           CaptureLink_CreateParams *pPrm,
                           UInt32 captureInWidth,
                           UInt32 captureInHeight,
                           AMV_VideoSensor_Id portId[],
                           UInt32 numOfSensor,
                           Bsp_VidSensorConfigRegs **vidSensorConfigRegs,
                           UInt32 *vidSensorConfigRegsSizes,
                           Bsp_VidSensorFlipParams flipParams[]
                           )
{

    if(chipType == FFN)
    {

        ChainsCommon_Amv_Cam_StartFFNCaptureDevice(    pCaptureSrc,
                                                       numOfSensor,
                                                       portId,
                                                       vidSensorConfigRegs,
                                                       vidSensorConfigRegsSizes,
                                                       flipParams);
    }
    else if(chipType == SCV)
    {

        UInt8 i;
        numOfSensor /= 2;
        captureInWidth *= 2;

        if(numOfSensor == 3)
        {
            ChainsCommon_Amv_StartSCDevices();
        }
        else
        {
            for (i=0; i < numOfSensor; i++)
            {

                switch(portId[i])
                {
                    case SCV_VIDEO_SENSOR_5_6:
                        ChainsCommon_Amv_StartCMSDevices();
                        break;
                    case SCV_VIDEO_SENSOR_1_2:
                    case SCV_VIDEO_SENSOR_3_4:
                        ChainsCommon_Amv_Start_PairTwoCam(SCV_VIDEO_SENSOR_1_2);
                        break;
                    case SCV_VIDEO_SENSOR_1_3:
                    case SCV_VIDEO_SENSOR_2_4:
                        ChainsCommon_Amv_Start_PairTwoCam(SCV_VIDEO_SENSOR_1_3);
                        break;
                    case SCV_VIDEO_SENSOR_1_4:
                    case SCV_VIDEO_SENSOR_2_3:
                        ChainsCommon_Amv_Start_PairTwoCam(SCV_VIDEO_SENSOR_1_4);
                        break;
                    default:
                        break;
                }
            }
        }
    }


   /* ChainsCommon_Amv_Cam_SetCapturePrms(
                                        pPrm,
                                        captureInWidth,
                                        captureInHeight,
                                        portId,
                                        numOfSensor
                                        );*/


}
