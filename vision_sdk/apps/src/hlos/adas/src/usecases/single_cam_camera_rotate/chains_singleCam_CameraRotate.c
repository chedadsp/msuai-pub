#include "chains_singleCam_CameraRotate_priv.h"
#include <src/hlos/adas/include/chains.h>
#include <src/hlos/common/chains_common.h>


#define CAPTURE_SENSOR_WIDTH      (1280)
#define CAPTURE_SENSOR_HEIGHT     (720)
#define NUMBER_OF_CAM             (2)
#define NUMBER_OF_PORT            (NUMBER_OF_CAM / 2)


/**
 *******************************************************************************
 *
 *  \brief  Use-case object
 *
 *        This structure contains all the LinksId's and create Params.
 *        The same is passed to all create, start, stop functions.
 *
 *******************************************************************************
*/
typedef struct {

    chains_singleCam_CameraRotateObj ucObj;

    UInt32  captureOutWidth;
    UInt32  captureOutHeight;
    UInt32  displayWidth;
    UInt32  displayHeight;

    Chains_Ctrl *chainsCfg;

    VideoSensorCreateAndStartAppPrm vidSensorPrm;

    UInt32  appCtrlLinkId;

    UInt32  displayActiveChId;
    /**< CH ID which is shown on display, by default 2x2 SW Mosaic
     *   is shown on display
     */
    UInt32  numLvdsCh;
    /**< Number of channels of LVDS to enable */

} Chains_singleCam_CameraRotateAppObj;

/**
 *******************************************************************************
 * \brief Run Time Menu string.
 *******************************************************************************
 */
char gChains_singleCam_CameraRotate_runTimeMenu[] = {
    "\r\n "
    "\r\n ===================="
    "\r\n Chains Run-time Menu"
    "\r\n ===================="
    "\r\n "
    "\r\n 0: Stop Chain"
    "\r\n "
    "\r\n 2: Pause Capture"
    "\r\n 3: Resume Capture"
    "\r\n "
    "\r\n p: Print Performance Statistics "
    "\r\n "
    "\r\n Enter Choice: "
    "\r\n "
};

static void chains_singleCam_CameraRotate_SetSelectPrm(
                                    SelectLink_CreateParams *pPrm)
{
    pPrm->numOutQue = 1;
    pPrm->outQueChInfo[0].outQueId   = 0;
    pPrm->outQueChInfo[0].numOutCh   = 1;
    pPrm->outQueChInfo[0].inChNum[0] = 0;
}


static Void chains_singleCam_CameraRotate_SetVpePrm(
                                    VpeLink_CreateParams *pPrm,
                                    UInt32 numLvdsCh)
{
    UInt32 chId;
    VpeLink_ChannelParams *chPrms;
    pPrm->enableOut[0] = TRUE;

    for(chId = 0; chId < numLvdsCh; chId++)
    {
        pPrm->chParams[chId].outParams[0].width
                = CAPTURE_SENSOR_WIDTH;

        pPrm->chParams[chId].outParams[0].height
                = CAPTURE_SENSOR_HEIGHT;

        pPrm->chParams[chId].outParams[0].dataFormat
                = SYSTEM_DF_YUV422I_YUYV; //SYSTEM_DF_YUV420SP_UV;

        pPrm->chParams[chId].outParams[0].numBufsPerCh = 8;

        chPrms = &pPrm->chParams[chId];

        chPrms->scCfg.bypass        = FALSE;
        chPrms->scCfg.nonLinear     = FALSE;
        chPrms->scCfg.stripSize     = 0;

        chPrms->scCropCfg.cropStartX    = 0;
        chPrms->scCropCfg.cropStartY    = 0;
        chPrms->scCropCfg.cropWidth     = CAPTURE_SENSOR_WIDTH;
        chPrms->scCropCfg.cropHeight    = CAPTURE_SENSOR_HEIGHT;
    }
}

/**
 *******************************************************************************
 *
 * \brief   Set link Parameters
 *
 *          It is called in Create function of the auto generated use-case file.
 *
 * \param pUcObj    [IN] Auto-generated usecase object
 * \param appObj    [IN] Application specific object
 *
 *******************************************************************************
*/
Void chains_singleCam_CameraRotate_SetAppPrms(chains_singleCam_CameraRotateObj *pUcObj, Void *appObj)
{
    Chains_singleCam_CameraRotateAppObj *pObj
        = (Chains_singleCam_CameraRotateAppObj*)appObj;

    UInt32 portId[VIDEO_SENSOR_MAX_LVDS_CAMERAS];

    pObj->displayActiveChId = 0;

    pObj->numLvdsCh = NUMBER_OF_CAM;

    pObj->captureOutWidth  = CAPTURE_SENSOR_WIDTH;
    pObj->captureOutHeight = CAPTURE_SENSOR_HEIGHT;

    ChainsCommon_GetDisplayWidthHeight(
        pObj->chainsCfg->displayType,
        &pObj->displayWidth,
        &pObj->displayHeight
        );


    portId[0] = SC_VIDEO_SENSOR_1_2;

    ChainsCommon_SetCapturePrms(
        &pUcObj->CapturePrm,
        CAPTURE_SENSOR_WIDTH * 2,
        CAPTURE_SENSOR_HEIGHT,
        portId,
        pObj->numLvdsCh / 2
        );

    chains_singleCam_CameraRotate_SetSelectPrm(&pUcObj->SelectPrm);

    chains_singleCam_CameraRotate_SetVpePrm(&pUcObj->VPEPrm, pUcObj->SelectPrm.outQueChInfo[0].numOutCh);

    ChainsCommon_SetGrpxSrcPrms(
        &pUcObj->GrpxSrcPrm,
        pObj->displayWidth,
        pObj->displayHeight
        );
    pUcObj->GrpxSrcPrm.grpxBufInfo.dataFormat = SYSTEM_DF_BGRA16_4444;

    ChainsCommon_SetDisplayPrms(
        &pUcObj->DisplayPrm,
        &pUcObj->Display_GrpxPrm,
        pObj->chainsCfg->displayType,
        pObj->displayWidth,
        pObj->displayHeight
        );

    ChainsCommon_StartDisplayCtrl(
        pObj->chainsCfg->displayType,
        pObj->displayWidth,
        pObj->displayHeight
        );

}


/**
 *******************************************************************************
 *
 * \brief   Start the capture display Links
 *
 *          Function sends a control command to capture and display link to
 *          to Start all the required links . Links are started in reverce
 *          order as information of next link is required to connect.
 *          System_linkStart is called with LinkId to start the links.
 *
 * \param   pObj  [IN] Chains_singleCam_CameraRotateObj
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
*/
static Void chains_singleCam_CameraRotate_StartApp(Chains_singleCam_CameraRotateAppObj *pObj)
{
    pObj->vidSensorPrm.captureSrcId = CHAINS_CAPTURE_SRC_OV10635;
    pObj->vidSensorPrm.isLVDSCaptMode = TRUE;
    pObj->vidSensorPrm.numLvdsCh = 2;
    pObj->vidSensorPrm.chipType = SCV;

    system("./ub964_SV.sh");

    System_linkControl(SYSTEM_LINK_ID_APP_CTRL,
                                APP_CTRL_LINK_CMD_VIDEO_SENSOR_CREATE_AND_START,
                                &pObj->vidSensorPrm,
                                sizeof(VideoSensorCreateAndStartAppPrm),
                                TRUE);

    ChainsCommon_statCollectorReset();
    ChainsCommon_memPrintHeapStatus();

    chains_singleCam_CameraRotate_Start(&pObj->ucObj);

    ChainsCommon_prfLoadCalcEnable(TRUE, FALSE, FALSE);

}


/**
 *******************************************************************************
 *
 * \brief   Delete the capture display Links
 *
 *          Function sends a control command to capture and display link to
 *          to delete all the prior created links
 *          System_linkDelete is called with LinkId to delete the links.
 *
 * \param   pObj   [IN]   Chains_singleCam_CameraRotateObj
 *
 *******************************************************************************
*/
static Void chains_singleCam_CameraRotate_StopAndDeleteApp(Chains_singleCam_CameraRotateAppObj *pObj)
{
    chains_singleCam_CameraRotate_Stop(&pObj->ucObj);
    chains_singleCam_CameraRotate_Delete(&pObj->ucObj);

    ChainsCommon_StopDisplayCtrl();

    System_linkControl(SYSTEM_LINK_ID_APP_CTRL,
                        APP_CTRL_LINK_CMD_VIDEO_SENSOR_STOP_AND_DELETE,
                        &pObj->vidSensorPrm,
                        sizeof(VideoSensorCreateAndStartAppPrm),
                        TRUE);


    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    ChainsCommon_prfLoadCalcEnable(FALSE, FALSE, FALSE);
}


/**
 *******************************************************************************
 *
 * \brief   Single Channel Capture Display usecase function
 *
 *          This functions executes the create, start functions
 *
 *          Further in a while loop displays run time menu and waits
 *          for user inputs to print the statistics or to end the demo.
 *
 *          Once the user inputs end of demo stop and delete
 *          functions are executed.
 *
 * \param   chainsCfg       [IN]   Chains_Ctrl
 *
 *******************************************************************************
*/
Void Chains_singleCam_CameraRotate(Chains_Ctrl *chainsCfg)
{
    char ch;
    UInt32 done = FALSE;
    Chains_singleCam_CameraRotateAppObj chainsObj;
    Int32 status;

    chainsObj.numLvdsCh         = 0; /* KW error fix */
    chainsObj.displayActiveChId = 0; /* KW error fix */

    chainsObj.chainsCfg = chainsCfg;

    chains_singleCam_CameraRotate_Create(&chainsObj.ucObj, &chainsObj);

    chains_singleCam_CameraRotate_StartApp(&chainsObj);

    while(!done)
    {
        Vps_printf(gChains_singleCam_CameraRotate_runTimeMenu);

        ch = Chains_readChar();

        switch(ch)
        {
            case '0':
                done = TRUE;
                break;

            case '2':
                status = System_linkStop(chainsObj.ucObj.CaptureLinkID);
                UTILS_assert(status == SYSTEM_LINK_STATUS_SOK);
                break;

            case '3':
                status = System_linkStart(chainsObj.ucObj.CaptureLinkID);
                UTILS_assert(status == SYSTEM_LINK_STATUS_SOK);
                break;

            case 'p':
            case 'P':
                ChainsCommon_PrintStatistics();
                chains_singleCam_CameraRotate_printStatistics(&chainsObj.ucObj);
                break;
            default:
                Vps_printf("\nUnsupported option '%c'. Please try again\n", ch);
                break;
        }
    }

    chains_singleCam_CameraRotate_StopAndDeleteApp(&chainsObj);
}