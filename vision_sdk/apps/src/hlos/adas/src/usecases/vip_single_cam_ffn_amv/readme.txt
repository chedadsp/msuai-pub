1. Copy vision_sdk/apps/tools/cameras_scripts/ub914_config.sh and vision_sdk/apps/tools/cameras_scripts/ov1063x_config.sh into rootfs/opt/vision_sdk.
2. Execute following commands before starting apps.out:
	> ./vision_sdk_load.sh
	> ./ub914_config.sh
	> ./ov1063x_config.sh
