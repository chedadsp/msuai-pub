#include "chains_twoCam_Alg_Display_priv.h"
#include <src/hlos/adas/include/chains.h>
#include <src/hlos/common/chains_common.h>


#define CAPTURE_SENSOR_WIDTH      (1280)
#define CAPTURE_SENSOR_HEIGHT     (720)
#define SCREEN_WIDTH              (1920)
#define SCREEN_HEIGHT             (1080)
#define NUMBER_OF_CAM             (2)
#define NUMBER_OF_PORT            (NUMBER_OF_CAM / 2)


/**
 *******************************************************************************
 *
 *  \brief  Use-case object
 *
 *        This structure contains all the LinksId's and create Params.
 *        The same is passed to all create, start, stop functions.
 *
 *******************************************************************************
*/
typedef struct {

    chains_twoCam_Alg_DisplayObj ucObj;

    UInt32  captureOutWidth;
    UInt32  captureOutHeight;
    UInt32  displayWidth;
    UInt32  displayHeight;

    Chains_Ctrl *chainsCfg;

    VideoSensorCreateAndStartAppPrm vidSensorPrm;

    UInt32  appCtrlLinkId;

    UInt32  displayActiveChId;
    /**< CH ID which is shown on display, by default 2x2 SW Mosaic
     *   is shown on display
     */
    UInt32  numLvdsCh;
    /**< Number of channels of LVDS to enable */

} Chains_twoCam_Alg_DisplayAppObj;

/**
 *******************************************************************************
 * \brief Run Time Menu string.
 *******************************************************************************
 */
char gChains_twoCam_Alg_Display_runTimeMenu[] = {
    "\r\n "
    "\r\n ===================="
    "\r\n Chains Run-time Menu"
    "\r\n ===================="
    "\r\n "
    "\r\n 0: Stop Chain"
    "\r\n "
    "\r\n 2: Pause Capture"
    "\r\n 3: Resume Capture"
    "\r\n "
    "\r\n p: Print Performance Statistics "
    "\r\n "
    "\r\n Enter Choice: "
    "\r\n "
};

/**
*******************************************************************************
*
* \brief    Set Select Create Parameters
*
*           Assign the CH to the Q
*
* \param    pPrm    [OUT]   SelectLink_CreateParams
*
*******************************************************************************
*/
static Void chains_twoCam_Alg_Display_SetSelectPrms(SelectLink_CreateParams *pPrm)
{
    pPrm->numOutQue = 2;

    pPrm->outQueChInfo[0].outQueId    = 0;
    pPrm->outQueChInfo[0].numOutCh    = 1;
    pPrm->outQueChInfo[0].inChNum[0]  = 0;

    pPrm->outQueChInfo[1].outQueId    = 1;
    pPrm->outQueChInfo[1].numOutCh    = 1;
    pPrm->outQueChInfo[1].inChNum[0]  = 1;
}


/**
 *******************************************************************************
 *
 * \brief   Set VPE Create Parameters
 *
 *          This function is used to set the VPE params.
 *          It is called in Create function. It is advisable to have
 *          chains_twoCam_Alg_Display_ResetLinkPrm prior to set params
 *          so all the default params get set.
 *          Scaling parameters and dataFormat are set.
 *
 *
 * \param   pPrm    [OUT]    VpeLink_CreateParams
 *
 *******************************************************************************
*/
static Void chains_twoCam_Alg_Display_SetVpePrm(
                    VpeLink_CreateParams *pPrm,
                    UInt32 displayWidth,
                    UInt32 displayHeight,
                    System_VideoDataFormat dataFormat
                    )
{
    UInt32 widthFactor, heightFactor;
    VpeLink_ChannelParams *chPrms;

    pPrm->enableOut[0] = TRUE;

    widthFactor  = 1;
    heightFactor = 1;

    pPrm->chParams[0].outParams[0].width
        = SystemUtils_floor(displayWidth/widthFactor, 16);

    pPrm->chParams[0].outParams[0].height
        = displayHeight/heightFactor;

    pPrm->chParams[0].outParams[0].dataFormat = dataFormat;
    pPrm->chParams[0].outParams[0].numBufsPerCh = 8;

    chPrms = &pPrm->chParams[0];

    chPrms->scCfg.bypass       = FALSE;
    chPrms->scCfg.nonLinear    = FALSE;
    chPrms->scCfg.stripSize    = 0;

    chPrms->scCropCfg.cropStartX = 0;
    chPrms->scCropCfg.cropStartY = 0;
    chPrms->scCropCfg.cropWidth  = CAPTURE_SENSOR_WIDTH;
    chPrms->scCropCfg.cropHeight = CAPTURE_SENSOR_HEIGHT;
}

/**
 *******************************************************************************
 *
 * \brief   Set link Parameters
 *
 *          It is called in Create function of the auto generated use-case file.
 *
 * \param pUcObj    [IN] Auto-generated usecase object
 * \param appObj    [IN] Application specific object
 *
 *******************************************************************************
*/
Void chains_twoCam_Alg_Display_SetAppPrms(chains_twoCam_Alg_DisplayObj *pUcObj, Void *appObj)
{
    Chains_twoCam_Alg_DisplayAppObj *pObj
        = (Chains_twoCam_Alg_DisplayAppObj*)appObj;

    UInt32 portId[VIDEO_SENSOR_MAX_LVDS_CAMERAS];

    pObj->displayActiveChId = 0;

    pObj->numLvdsCh = NUMBER_OF_CAM;

    pObj->captureOutWidth  = CAPTURE_SENSOR_WIDTH;
    pObj->captureOutHeight = CAPTURE_SENSOR_HEIGHT;

    ChainsCommon_GetDisplayWidthHeight(
        pObj->chainsCfg->displayType,
        &pObj->displayWidth,
        &pObj->displayHeight
        );

    portId[0] = SC_VIDEO_SENSOR_1_2;

    ChainsCommon_SetCapturePrms(
        &pUcObj->CapturePrm,
        CAPTURE_SENSOR_WIDTH * 2,
        CAPTURE_SENSOR_HEIGHT,
        portId,
        pObj->numLvdsCh / 2
        );

    chains_twoCam_Alg_Display_SetSelectPrms(&pUcObj->SelectPrm);

    ChainsCommon_SetGrpxSrcPrms( 
        &pUcObj->GrpxSrcPrm, 
        pObj->displayWidth, 
        pObj->displayHeight 
        ); 
 
    ChainsCommon_SetDisplayPrms( 
        &pUcObj->Display_C2GPrm, 
        &pUcObj->Display_GrpxPrm, 
        pObj->chainsCfg->displayType, 
        pObj->displayWidth, 
        pObj->displayHeight 
        ); 

    pUcObj->Display_C2GPrm.rtParams.posX        = (float)0; 
    pUcObj->Display_C2GPrm.rtParams.posY        = (float)0; 
    pUcObj->Display_C2GPrm.rtParams.tarWidth    = (float)pObj->displayWidth; 
    pUcObj->Display_C2GPrm.rtParams.tarHeight   = (float)(pObj->displayHeight/2); 
    pUcObj->Display_C2GPrm.displayId            = DISPLAY_LINK_INST_DSS_VID1; 
 
    pUcObj->Display_CRPrm.rtParams.posX       = (float)0; 
    pUcObj->Display_CRPrm.rtParams.posY       = (float)(pObj->displayHeight/2); 
    pUcObj->Display_CRPrm.rtParams.tarWidth   = (float)pObj->displayWidth; 
    pUcObj->Display_CRPrm.rtParams.tarHeight  = (float)(pObj->displayHeight/2); 
    pUcObj->Display_CRPrm.displayId           = DISPLAY_LINK_INST_DSS_VID2; 
 
    chains_twoCam_Alg_Display_SetVpePrm( 
        &pUcObj->VPE_1Prm, 
        pObj->displayWidth, 
        pObj->displayHeight/2, 
        SYSTEM_DF_YUV422I_YUYV 
        ); 
 
    chains_twoCam_Alg_Display_SetVpePrm( 
        &pUcObj->VPE_2Prm, 
        pObj->displayWidth, 
        pObj->displayHeight/2, 
        SYSTEM_DF_YUV422I_YUYV
        );

    ChainsCommon_StartDisplayCtrl(
        pObj->chainsCfg->displayType,
        pObj->displayWidth,
        pObj->displayHeight
        );

}


/**
 *******************************************************************************
 *
 * \brief   Start the capture display Links
 *
 *          Function sends a control command to capture and display link to
 *          to Start all the required links . Links are started in reverce
 *          order as information of next link is required to connect.
 *          System_linkStart is called with LinkId to start the links.
 *
 * \param   pObj  [IN] Chains_twoCam_Alg_DisplayObj
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
*/
static Void chains_twoCam_Alg_Display_StartApp(Chains_twoCam_Alg_DisplayAppObj *pObj)
{
    pObj->vidSensorPrm.captureSrcId = CHAINS_CAPTURE_SRC_OV10635;
    pObj->vidSensorPrm.isLVDSCaptMode = TRUE;
    pObj->vidSensorPrm.numLvdsCh = 2;
    pObj->vidSensorPrm.chipType = SCV;

    system("./ub964_SV.sh");

    System_linkControl(SYSTEM_LINK_ID_APP_CTRL,
                                APP_CTRL_LINK_CMD_VIDEO_SENSOR_CREATE_AND_START,
                                &pObj->vidSensorPrm,
                                sizeof(VideoSensorCreateAndStartAppPrm),
                                TRUE);

    ChainsCommon_statCollectorReset();
    ChainsCommon_memPrintHeapStatus();

    chains_twoCam_Alg_Display_Start(&pObj->ucObj);

    ChainsCommon_prfLoadCalcEnable(TRUE, FALSE, FALSE);

}


/**
 *******************************************************************************
 *
 * \brief   Delete the capture display Links
 *
 *          Function sends a control command to capture and display link to
 *          to delete all the prior created links
 *          System_linkDelete is called with LinkId to delete the links.
 *
 * \param   pObj   [IN]   Chains_twoCam_Alg_DisplayObj
 *
 *******************************************************************************
*/
static Void chains_twoCam_Alg_Display_StopAndDeleteApp(Chains_twoCam_Alg_DisplayAppObj *pObj)
{
    chains_twoCam_Alg_Display_Stop(&pObj->ucObj);
    chains_twoCam_Alg_Display_Delete(&pObj->ucObj);

    ChainsCommon_StopDisplayCtrl();

    System_linkControl(SYSTEM_LINK_ID_APP_CTRL,
                        APP_CTRL_LINK_CMD_VIDEO_SENSOR_STOP_AND_DELETE,
                        &pObj->vidSensorPrm,
                        sizeof(VideoSensorCreateAndStartAppPrm),
                        TRUE);


    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    ChainsCommon_prfLoadCalcEnable(FALSE, FALSE, FALSE);
}

/**
 *******************************************************************************
 *
 * \brief   Single Channel Capture Display usecase function
 *
 *          This functions executes the create, start functions
 *
 *          Further in a while loop displays run time menu and waits
 *          for user inputs to print the statistics or to end the demo.
 *
 *          Once the user inputs end of demo stop and delete
 *          functions are executed.
 *
 * \param   chainsCfg       [IN]   Chains_Ctrl
 *
 *******************************************************************************
*/
Void Chains_twoCam_Alg_Display(Chains_Ctrl *chainsCfg)
{
    char ch;
    UInt32 done = FALSE;
    Chains_twoCam_Alg_DisplayAppObj chainsObj;
    Int32 status;

    chainsObj.numLvdsCh         = 0;
    chainsObj.displayActiveChId = 0;

    chainsObj.chainsCfg = chainsCfg;

    chains_twoCam_Alg_Display_Create(&chainsObj.ucObj, &chainsObj);

    chains_twoCam_Alg_Display_StartApp(&chainsObj);

    while(!done)
    {
        Vps_printf(gChains_twoCam_Alg_Display_runTimeMenu);

        ch = Chains_readChar();

        switch(ch)
        {
            case '0':
                done = TRUE;
                break;

            case '2':
                status = System_linkStop(chainsObj.ucObj.CaptureLinkID);
                UTILS_assert(status == SYSTEM_LINK_STATUS_SOK);
                break;

            case '3':
                status = System_linkStart(chainsObj.ucObj.CaptureLinkID);
                UTILS_assert(status == SYSTEM_LINK_STATUS_SOK);
                break;

            case 'p':
            case 'P':
                ChainsCommon_PrintStatistics();
                chains_twoCam_Alg_Display_printStatistics(&chainsObj.ucObj);
                break;
            default:
                Vps_printf("\nUnsupported option '%c'. Please try again\n", ch);
                break;
        }
    }

    chains_twoCam_Alg_Display_StopAndDeleteApp(&chainsObj);
}