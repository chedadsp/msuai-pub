/*******************************************************************************
 *
 * Copyright (c) 2019  RT-RK Automotive d.o.o
 * Narodnog fronta 23 D-E, 21000 Novi Sad, Serbia
 *
 * All Rights Reserved
 *
 * P R O P R I E T A R Y & C O N F I D E N T I A L
 *
 * -----------------------------------------------------
 * http://www.rt-rk.com
 * -----------------------------------------------------
 *
 ******************************************************************************/

/**
 *******************************************************************************
 * @file chains_multiCam_frameGrab_display.c
 * 
 * @author Uros Zivkovic (uros.zivkovic@rt-rk.com)
 * 
 * @brief Usecase file implementation of Multi-Ch Capture + Frame Grab, Display
 *        Used to write capture frames to shared memroy and display it using
 *        weston port of wayland compositor.
 *
 *        The data flow daigram is shown below
 *
 *                 Capture (6CH 30fps 1280x720)   DispDistSrc (weston)
 *                   |                                |
 *                 Split                          Display (M4)
 *                   |
 *                  Dup
 *                   |
 *               FrameGrab
 * 
 * @version 0.0 (Oct 2018) : [MD] First version
 * @version 0.1 (Mar 2019) : [UZ] Updated file structure and
 *                                adjusted file contingency
 * 
 * @date 25-03-2019
 * 
 * @copyright Copyright (c) 2019 RT-RK Automotive, All rights reserved.
 * 
 *******************************************************************************
 */

/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include "chains_multiCam_frameGrab_display_priv.h"
#include <src/hlos/adas/include/chains.h>
#include <src/hlos/common/chains_common.h>

#define MAX_NUMBER_OF_CHANNELS    (1)

#define CAPTURE_SENSOR_WIDTH      (1280)
#define CAPTURE_SENSOR_HEIGHT     (720)

/**
 *******************************************************************************
 * \brief Semaphore paths for shared memory reading/writing sync
 *******************************************************************************
 */
#define SEM_PATH_0 "tmp_shmsem0"
#define SEM_PATH_1 "tmp_shmsem1"
#define SEM_PATH_2 "tmp_shmsem2"
#define SEM_PATH_3 "tmp_shmsem3"
#define SEM_PATH_4 "tmp_shmsem4"
#define SEM_PATH_5 "tmp_shmsem5"

/**
 *******************************************************************************
 * \brief Semaphore keys for shared memory reading/writing sync
 *******************************************************************************
 */
#define SHM_KEY_0                   (1111)
#define SHM_KEY_1                   (2222)
#define SHM_KEY_2                   (3333)
#define SHM_KEY_3                   (4444)
#define SHM_KEY_4                   (5555)
#define SHM_KEY_5                   (6666)

#define V_CONTROLLER_NAME           "/dev/vdrm-controller-0"
#define VDRM_CRTC                   (26)

/**
 *******************************************************************************
 * \brief Channels with timestamp difference <= SYNC_DELTA_IN_MSEC
 *        are synced together by sync link
 *******************************************************************************
 */
#define SYNC_DELTA_IN_MSEC          (16)

/**
 *******************************************************************************
 * \brief Channels with timestamp older than SYNC_DROP_THRESHOLD_IN_MSEC
 *        are dropped by sync link
 *******************************************************************************
 */
#define SYNC_DROP_THRESHOLD_IN_MSEC (33)

/**
 *******************************************************************************
 *
 *  \brief  Use-case object
 *
 *        This structure contains all the LinksId's and create Params.
 *        The same is passed to all create, start, stop functions.
 *
 *******************************************************************************
*/
typedef struct {

    chains_multiCam_frameGrab_displayObj ucObj;

    UInt32  displayWidth;
    UInt32  displayHeight;

    VideoSensorCreateAndStartAppPrm vidSensorPrm;

    Chains_Ctrl *chainsCfg;

    UInt32  numLvdsCh;
    /**< Number of channels of LVDS to enable */

    UInt32  displayActiveChId;
    /**< CH ID which is shown on display */

    UInt32 defaultCh;

} chains_multiCam_frameGrab_display_AppObj;

/**
 *******************************************************************************
 *
 * \brief   Set Capture Create Parameters for multi camera capture mode
 *
 * \param   pPrm             [IN] Capture link public object    
 * \param   captureInWidth   [IN] Input width                   
 * \param   captureInHeight  [IN] Input height                  
 * \param   portId           [IN] Input port ID                 
 * \param   numLvdsCh        [IN] Number of LVDS channels       
 *******************************************************************************
*/
Void chains_multiCam_frameGrab_display_SetCapturePrms(
                        CaptureLink_CreateParams *pPrm,
                        UInt32 captureInWidth,
                        UInt32 captureInHeight,
                        UInt32 portId[],
                        UInt32 numLvdsCh
                        )
{
    UInt32 i, streamId;

    CaptureLink_VipInstParams *pInstPrm;
    CaptureLink_InParams *pInprms;
    CaptureLink_OutParams *pOutprms;
    CaptureLink_VipScParams *pScPrm;
    CaptureLink_VipPortConfig    *pPortCfg;

    memset(pPrm, 0, sizeof(*pPrm));

    pPrm->numVipInst = numLvdsCh;

    for (i=0; i<SYSTEM_CAPTURE_VIP_INST_MAX; i++)
    {
        pInstPrm = &pPrm->vipInst[i];

        UTILS_assert(portId != NULL);
        pInstPrm->vipInstId     =   portId[i];

        pInstPrm->videoIfMode   =   SYSTEM_VIFM_SCH_DS_HSYNC_VSYNC;
        pInstPrm->videoIfWidth  =   SYSTEM_VIFW_8BIT;
        pInstPrm->bufCaptMode   =   SYSTEM_CAPT_BCM_FRM_DROP;
        pInstPrm->numStream     =   1;

        pInprms = &pInstPrm->inParams;

        pInprms->width      =   captureInWidth;
        pInprms->height     =   captureInHeight;
        pInprms->dataFormat =   SYSTEM_DF_YUV422P;
        pInprms->scanFormat =   SYSTEM_SF_PROGRESSIVE;

        for (streamId = 0; streamId < CAPTURE_LINK_MAX_OUTPUT_PER_INST;
                streamId++)
        {
            pOutprms = &pInstPrm->outParams[streamId];
            pOutprms->width         =   pInprms->width;
            pOutprms->height        =   pInprms->height;
            pOutprms->dataFormat    =   SYSTEM_DF_YUV420SP_UV; //SYSTEM_DF_YUV420SP_UV SYSTEM_DF_BGR24_888
            pOutprms->maxWidth      =   pOutprms->width;
            pOutprms->maxHeight     =   pOutprms->height;
            pOutprms->scEnable      =   FALSE;

            /* sub-frame not supported, set to FALSE */
            pOutprms->subFrmPrms.subFrameEnable = FALSE;
            pOutprms->subFrmPrms.numLinesPerSubFrame = 0;

        }
        pScPrm = &pInstPrm->scPrms;
        pScPrm->inCropCfg.cropStartX = 0;
        pScPrm->inCropCfg.cropStartY = 0;
        pScPrm->inCropCfg.cropWidth = pInprms->width;
        pScPrm->inCropCfg.cropHeight = pInprms->height;

        pScPrm->scCfg.bypass       = FALSE;
        pScPrm->scCfg.nonLinear    = FALSE;
        pScPrm->scCfg.stripSize    = 0;

        pScPrm->userCoeff = FALSE;

        /* pScPrm->scCoeffCfg is not reuquired in case
         * pScPrm->userCoeff is FALSE
         */
        pPortCfg = &pInstPrm->vipPortCfg;
        pPortCfg->syncType          =   SYSTEM_VIP_SYNC_TYPE_DIS_SINGLE_YUV;
        pPortCfg->ancCropEnable     =   FALSE;

        pPortCfg->intfCfg.clipActive    =   FALSE;
        pPortCfg->intfCfg.clipBlank     =   FALSE;
        pPortCfg->intfCfg.intfWidth     =   SYSTEM_VIFW_16BIT;

        pPortCfg->disCfg.fidSkewPostCnt     =   0;
        pPortCfg->disCfg.fidSkewPreCnt      =   0;
        pPortCfg->disCfg.lineCaptureStyle   =
                                SYSTEM_VIP_LINE_CAPTURE_STYLE_ACTVID;
        pPortCfg->disCfg.fidDetectMode      =   SYSTEM_VIP_FID_DETECT_MODE_PIN;
        pPortCfg->disCfg.actvidPol          =   SYSTEM_POL_HIGH;
        pPortCfg->disCfg.vsyncPol           =   SYSTEM_POL_HIGH;
        pPortCfg->disCfg.hsyncPol           =   SYSTEM_POL_HIGH;
        pPortCfg->disCfg.discreteBasicMode  =   TRUE;

        pPortCfg->comCfg.ctrlChanSel        =   SYSTEM_VIP_CTRL_CHAN_SEL_7_0;
        pPortCfg->comCfg.ancChSel8b         =
                            SYSTEM_VIP_ANC_CH_SEL_8B_LUMA_SIDE;
        pPortCfg->comCfg.pixClkEdgePol      =   SYSTEM_EDGE_POL_RISING;
        pPortCfg->comCfg.invertFidPol       =   FALSE;
        pPortCfg->comCfg.enablePort         =   FALSE;
        pPortCfg->comCfg.expectedNumLines   =   pInprms->height;
        pPortCfg->comCfg.expectedNumPix     =   pInprms->width;
        pPortCfg->comCfg.repackerMode       =   SYSTEM_VIP_REPACK_CBA_TO_CBA;

        pPortCfg->actCropEnable                         =   TRUE;
        pPortCfg->actCropCfg.srcNum                     =   0;
        pPortCfg->actCropCfg.cropCfg.cropStartX         =   0;
        pPortCfg->actCropCfg.cropCfg.cropStartY         =   0;
        pPortCfg->actCropCfg.cropCfg.cropWidth          =   pInprms->width;
        pPortCfg->actCropCfg.cropCfg.cropHeight         =   pInprms->height;

        pPortCfg->ancCropCfg.srcNum                     =   0;
        pPortCfg->ancCropCfg.cropCfg.cropStartX         =   0;
        pPortCfg->ancCropCfg.cropCfg.cropStartY         =   0;
        pPortCfg->ancCropCfg.cropCfg.cropWidth          =   0;
        pPortCfg->ancCropCfg.cropCfg.cropHeight         =   0;

        pInstPrm->numBufs = CAPTURE_LINK_NUM_BUFS_PER_CH_DEFAULT;
    }
}

/**
 *******************************************************************************
 *
 * \brief   Set FrameGrab Create Parameters
 *
 * \param   pPrm             [IN] Link public object     
 * \param   semPath          [IN] Semaphore path         
 * \param   sharedMemkey     [IN] Semaphore key          
 * \param   defaultChannel   [IN] Default channel        
 *******************************************************************************
*/
Void chains_multiCam_frameGrab_display_SetFrameGrabPrms(
                        FrameGrabLink_CreateParams  *pPrm,
                        char* semPath,
                        key_t sharedMemkey,
                        UInt32 defaultChannel)
{
    pPrm->shmKey = sharedMemkey;
    strcpy(pPrm->semPath, semPath);
    pPrm->defaultCh = defaultChannel;
}

/**
 *******************************************************************************
 *
 * \brief   Set link Parameters
 *
 *          It is called in Create function of the auto generated use-case file.
 *
 * \param pUcObj    [IN] Auto-generated usecase object
 * \param appObj    [IN] Application specific object
 *
 *******************************************************************************
*/
Void chains_multiCam_frameGrab_display_SetAppPrms(chains_multiCam_frameGrab_displayObj *pUcObj, Void *appObj)
{
    chains_multiCam_frameGrab_display_AppObj *pObj
        = (chains_multiCam_frameGrab_display_AppObj*)appObj;

    ChainsCommon_GetDisplayWidthHeight(
        pObj->chainsCfg->displayType,
        &pObj->displayWidth,
        &pObj->displayHeight
        );

    UInt32 portId[VIDEO_SENSOR_MAX_LVDS_CAMERAS];

    portId[2] = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTA;
    portId[1] = SYSTEM_CAPTURE_INST_VIP1_SLICE1_PORTB;
    portId[0] = SYSTEM_CAPTURE_INST_VIP3_SLICE2_PORTA;

    pObj->numLvdsCh = 6;
    pObj->displayActiveChId = 0;

    chains_multiCam_frameGrab_display_SetCapturePrms(
        &pUcObj->CapturePrm,
        CAPTURE_SENSOR_WIDTH * 2, /**From one port 2 frames come */
        CAPTURE_SENSOR_HEIGHT,
        portId,
        pObj->numLvdsCh / 2 /**Three ports with 2 cameras each */
        );

    UInt32 displayWidth, displayHeight;
    DisplayLink_CreateParams *pPrm_display;

    ChainsCommon_GetDisplayWidthHeight(
        pObj->chainsCfg->displayType,
        &displayWidth,
        &displayHeight
        );

    sprintf((char *)pUcObj->DispDistSrc_westonPrm.vDrmControllerName, V_CONTROLLER_NAME);
    pUcObj->DispDistSrc_westonPrm.vDrmControllerObjectId = VDRM_CRTC;
    pUcObj->DispDistSrc_westonPrm.vDrmControllerObjectType = DISP_DIST_SRC_LINK_DRM_OBJECT_TYPE_CRTC;
    pUcObj->DispDistSrc_westonPrm.width = displayWidth;
    pUcObj->DispDistSrc_westonPrm.height = displayHeight;
    pUcObj->DispDistSrc_westonPrm.format = SYSTEM_DF_BGRX24_8888;

    pPrm_display = &pUcObj->Display_m4Prm;
    pPrm_display->rtParams.tarWidth         = displayWidth;
    pPrm_display->rtParams.tarHeight        = displayHeight;
    pPrm_display->rtParams.posX             = 0;
    pPrm_display->rtParams.posY             = 0;
    pPrm_display->displayId                 = DISPLAY_LINK_INST_DSS_GFX1;

    ChainsCommon_StartDisplayCtrl(
        pObj->chainsCfg->displayType,
        displayWidth,
        displayHeight
        );

    chains_multiCam_frameGrab_display_SetFrameGrabPrms(
        &pUcObj->FrameGrab_0Prm,
        SEM_PATH_0,
        SHM_KEY_0,
        0
        );

    chains_multiCam_frameGrab_display_SetFrameGrabPrms(
        &pUcObj->FrameGrab_1Prm,
        SEM_PATH_1,
        SHM_KEY_1,
        1
        );


    chains_multiCam_frameGrab_display_SetFrameGrabPrms(
        &pUcObj->FrameGrab_2Prm,
        SEM_PATH_2,
        SHM_KEY_2,
        2
        );

    chains_multiCam_frameGrab_display_SetFrameGrabPrms(
        &pUcObj->FrameGrab_3Prm,
        SEM_PATH_3,
        SHM_KEY_3,
        3
        );

    chains_multiCam_frameGrab_display_SetFrameGrabPrms(
        &pUcObj->FrameGrab_4Prm,
        SEM_PATH_4,
        SHM_KEY_4,
        4
        );


    chains_multiCam_frameGrab_display_SetFrameGrabPrms(
        &pUcObj->FrameGrab_5Prm,
        SEM_PATH_5,
        SHM_KEY_5,
        5
        );
}

/**
 *******************************************************************************
 *
 * \brief   Start the capture display Links
 *
 *          Function sends a control command to capture and display link to
 *          to Start all the required links . Links are started in reverce
 *          order as information of next link is required to connect.
 *          System_linkStart is called with LinkId to start the links.
 *
 * \param   pObj  [IN] Application specific object
 *
 * \return  SYSTEM_LINK_STATUS_SOK on success
 *
 *******************************************************************************
*/
Void chains_multiCam_frameGrab_display_StartApp(chains_multiCam_frameGrab_display_AppObj *pObj)
{

    pObj->vidSensorPrm.captureSrcId = CHAINS_CAPTURE_SRC_OV10635;
    pObj->vidSensorPrm.isLVDSCaptMode = TRUE;
    pObj->vidSensorPrm.numLvdsCh = 6;
    pObj->vidSensorPrm.chipType = SCV;

    system("./ub964_6cam_SC.sh");

    System_linkControl(SYSTEM_LINK_ID_APP_CTRL,
                                APP_CTRL_LINK_CMD_VIDEO_SENSOR_CREATE_AND_START,
                                &pObj->vidSensorPrm,
                                sizeof(VideoSensorCreateAndStartAppPrm),
                                TRUE);

    ChainsCommon_statCollectorReset();
    ChainsCommon_memPrintHeapStatus();

    chains_multiCam_frameGrab_display_Start(&pObj->ucObj);
}

/**
 *******************************************************************************
 *
 * \brief   Delete the capture display Links
 *
 *          Function sends a control command to capture and display link to
 *          to delete all the prior created links
 *          System_linkDelete is called with LinkId to delete the links.
 *
 * \param   pObj   [IN] Application specific object
 *
 *******************************************************************************
*/
Void chains_multiCam_frameGrab_display_StopAndDeleteApp(chains_multiCam_frameGrab_display_AppObj *pObj)
{
    chains_multiCam_frameGrab_display_Stop(&pObj->ucObj);
    chains_multiCam_frameGrab_display_Delete(&pObj->ucObj);

    ChainsCommon_StopDisplayCtrl();

    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    ChainsCommon_prfLoadCalcEnable(FALSE, TRUE, TRUE);
}

/**
 *******************************************************************************
 *
 * \brief   Single Channel Capture Display usecase function
 *
 *          This functions executes the create, start functions
 *
 *          Further in a while loop displays run time menu and waits
 *          for user inputs to print the statistics or to end the demo.
 *
 *          Once the user inputs 'end of demo' stop and delete
 *          functions are executed.
 *
 * \param   chainsCfg       [IN] Usecase chain configuration object
 *
 *******************************************************************************
*/
Void chains_multiCam_frameGrab_display(Chains_Ctrl *chainsCfg)
{
    char ch = 'a';
    UInt32 done = FALSE;
    chains_multiCam_frameGrab_display_AppObj chainsObj;

    chainsObj.numLvdsCh = 0;
    chainsObj.displayActiveChId = 0;

    chainsObj.chainsCfg = chainsCfg;

    chains_multiCam_frameGrab_display_Create(&chainsObj.ucObj, &chainsObj);

    chains_multiCam_frameGrab_display_StartApp(&chainsObj);

	done = FALSE;
    while(!done)
    {
        ch = '1';
        ch = Chains_menuRunTime();
        switch(ch)
        {
            case '0':
                done = TRUE;

                break;
            case 'p':
            case 'P':
                ChainsCommon_PrintStatistics();
                chains_multiCam_frameGrab_display_printStatistics(&chainsObj.ucObj);
                break;
            default:
                Vps_printf("\nUnsupported option '%c'. Please try again\n", ch);
                break;
        }
    }

    chains_multiCam_frameGrab_display_StopAndDeleteApp(&chainsObj);

}
