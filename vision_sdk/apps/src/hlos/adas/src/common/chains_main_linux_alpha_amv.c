/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include <system_cfg.h>
#include <src/hlos/adas/include/chains.h>

char gChains_menuAlphaAMV_Usecases[] = {
    "\r\n "
    "\r\n Alpha AMV Board Usecases"
    "\r\n ------------------------"
    "\r\n Excercise 1:"
    "\r\n [a]: Tutorial Capture Display"
    "\r\n Excercise 2:"
    "\r\n [b]: Tutorial ColorToGray algorithm"
    "\r\n Excercise 3:"
    "\r\n [c]: Camera Rotate Algorithm Link"
    "\r\n Excercise 4:"
    "\r\n [d]: Two Camera Alg Display"
    "\r\n [e]: One Camera - Two Alg - Display"
    "\r\n Excercise 5:"
    "\r\n [f]: NullSrc + Decode + Display"
    "\r\n [g]: Lane detection example"
    "\r\n [h]: NullSrc + Decode + Display + Lane detection"
    "\r\n "
    "\r\n x: Exit"
    "\r\n "
    "\r\n Enter Choice: "
    "\r\n "
};

/**
 *******************************************************************************
 *
 * \brief   Function to select demo depending on user input
 *          Accepts user input as ch and switches to corrosponding usecase
 *
 * \param   ch        [IN]  Input choise for user
 *
 *******************************************************************************
*/
Void Chains_menuAlphaAMVRun()
{
    
    char ch;
    Bool done = FALSE;

    while(!done)
    {

        Vps_printf(gChains_menuAlphaAMV_Usecases);
        ch = Chains_readChar();
        Vps_printf(" \r\n");

        switch(ch)
        {
            case 'a':
                Chains_tutorialTwoCam_Display(&gChains_usecaseCfg);
            break;
            case 'b':
                Chains_vipSingleCam_ColorToGray(&gChains_usecaseCfg);
            break;
            case 'c':
                Chains_singleCam_CameraRotate(&gChains_usecaseCfg);
            break;
            case 'd':
                Chains_twoCam_Alg_Display(&gChains_usecaseCfg);
            break;
            case 'e':
                // Chains_oneCam_TwoAlg_Display(&gChains_usecaseCfg);
            break;
            case 'f':
                Chains_nullSrc_DecodeDisplay(&gChains_usecaseCfg);
            break;
            case 'g':
                Chains_vipSingleCameraLaneDetect(&gChains_usecaseCfg);
            break;
            case 'h':
                // Chains_nullSrc_LD_Display(&gChains_usecaseCfg);
            break;
            case 'x':
            case 'X':
                done = TRUE;
                break;

            default:
                Vps_printf(" Unsupported option '%c'. Please try again\n", ch);
                break;
        }
    }
}
