/******************************************************************************
Copyright (c) [2012 - 2017] Texas Instruments Incorporated

All rights reserved not granted herein.

Limited License.

 Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 license under copyrights and patents it now or hereafter owns or controls to
 make,  have made, use, import, offer to sell and sell ("Utilize") this software
 subject to the terms herein.  With respect to the foregoing patent license,
 such license is granted  solely to the extent that any such patent is necessary
 to Utilize the software alone.  The patent license shall not apply to any
 combinations which include this software, other than combinations with devices
 manufactured by or for TI ("TI Devices").  No hardware patent is licensed
 hereunder.

 Redistributions must preserve existing copyright notices and reproduce this
 license (including the above copyright notice and the disclaimer and
 (if applicable) source code license limitations below) in the documentation
 and/or other materials provided with the distribution

 Redistribution and use in binary form, without modification, are permitted
 provided that the following conditions are met:

 * No reverse engineering, decompilation, or disassembly of this software
   is permitted with respect to any software provided in binary form.

 * Any redistribution and use are licensed by TI for use only with TI Devices.

 * Nothing shall obligate TI to provide you with source code for the software
   licensed and provided to you in object code.

 If software source code is provided to you, modification and redistribution of
 the source code are permitted provided that the following conditions are met:

 * Any redistribution and use of the source code, including any resulting
   derivative works, are licensed by TI for use only with TI Devices.

 * Any redistribution and use of any object code compiled from the source code
   and any resulting derivative works, are licensed by TI for use only with TI
   Devices.

 Neither the name of Texas Instruments Incorporated nor the names of its
 suppliers may be used to endorse or promote products derived from this software
 without specific prior written permission.

 DISCLAIMER.

 THIS SOFTWARE IS PROVIDED BY TI AND TI�S LICENSORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL TI AND TI�S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/

/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */

#include <system_cfg.h>
#include <src/hlos/adas/include/chains.h>
#include <include/link_api/networkCtrl_api.h>

/*******************************************************************************
 *  Globals
 *******************************************************************************
 */

Void Chains_showSystemSettingsMenu();
Void Chains_menuSingleCameraRun();
Void Chains_menuMultiCameraLvdsRun();
Void Chains_menuOpenComputeRun();
Void Chains_menuAvbRxRun();
Void Chains_menuIssRun();
Void Chains_menuAlphaAMVRun();
Void Chains_tidl(Chains_Ctrl *chainsCfg);

Chains_Ctrl gChains_usecaseCfg;

/**
 *******************************************************************************
 * \brief Run Time Test Menu string.
 *******************************************************************************
 */
char gChains_testMenu[] = {
    "\r\n "
    "\r\n ===================="
    "\r\n Vision SDK Usecases"
    "\r\n ===================="
    "\r\n 1: Single Camera Usecases"
    "\r\n 2: Multi-Camera LVDS Usecases"
    "\r\n 3: Open-Compute Usecases"
#ifdef AVB_INCLUDE
    "\r\n 4: AVB RX Usecases, (TDA2x & TDA2Ex ONLY)"
#endif
#ifdef ISS_INCLUDE
    "\r\n 5: ISS Usecases, (TDA2Px ONLY)"
#endif
    "\r\n "
    "\r\n a: ALPHA AMV Usecases"
#ifdef UC_tidl
    "\r\n t: Tidl"
#endif
    "\r\n "
    "\r\n p: CPU Status"
    "\r\n i: Show IP Addr (IPU + NDK + AVB) "
    "\r\n "
    "\r\n s: System Settings "
    "\r\n "
    "\r\n x: Exit "
    "\r\n z: Exit - AND SHUTDOWN Remote CPUs"
    "\r\n "
    "\r\n Enter Choice: "
    "\r\n "
};

/**
 *******************************************************************************
 *
 * \brief   Application's main - entry function
 *
 *******************************************************************************
*/
Int32 main (Int32 argc, Char ** argv)
{
    Bool shutdownRemoteCores = FALSE;

    System_init();
#ifndef QNX_BUILD
    NetworkCtrl_init();
#endif
    ChainsCommon_appCtrlCommonInit();
    Chains_Ctrl_Init(&gChains_usecaseCfg);

    /* allow remote prints to complete, before showing main menu */
    OSA_waitMsecs(500);

    gChains_usecaseCfg.ispOpMode = ISSM2MISP_LINK_OPMODE_12BIT_LINEAR;
    strncpy(gChains_usecaseCfg.sensorName, SENSOR_OMNIVISION_OV10640_IMI,
        ISS_SENSORS_MAX_NAME);

    Chains_menuAlphaAMVRun();

    ChainsCommon_appCtrlCommonDeInit();
#ifndef QNX_BUILD
    NetworkCtrl_deInit();
#endif
    System_deInit(shutdownRemoteCores);

    return 0;
}

