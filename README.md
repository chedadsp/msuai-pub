**Important!** It is recommended to use provided Ubuntu VM.

**Build** instructions:

```
cd ~/PROCESSOR_SDK_VISION_03_04_00_00/vision_sdk/build

make -s -j -k depend
make -s -j -k

```

**Download** & **connect** instructions:

```
./scopy_to_fs.sh 192.168.XXX.XXX

ssh root@192.168.XXX.XXX

```

**Running** example application on Alpha board:

```
cd /opt/vision_sdk

./vision_sdk_load.sh
./apps.out

```

If you need to add some additional files make sure to update .gitignore. Please try to keep git repo minimal.


<details><summary>Build **preparation** for clean Ubuntu 18.04</summary>

Build assumes ubuntu user rtrk. 

For running build this a different user, one need to fix relevant scripting involved. Good luck!

```
git clone to ~/PROCESSOR_SDK_VISION_03_04_00_00/
download \\samba03\nastavni_materijali\dosivuav\laboratorijske_vezbe\PROCESSOR_SDK_VISION_03_04_00_00.tar.xz
untar to ~/ (this will overwrite some files)
git reset --hard
```

</details>


